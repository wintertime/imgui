#include "IMGUI/Core/GenID.hpp"
#include "UnitTest++.h"
#include <set>
#include <map>
#include <iostream>

namespace IMGUI {

SUITE(GenID) {

TEST(zeroParent) {
  CHECK(GenID(0,1,0)!=0);
  CHECK(GenID(0,1,0)!=-1);
  CHECK(GenID(0,1,1)!=0);
  CHECK(GenID(0,1,1)!=-1);
}

TEST(negOneParent) {
  CHECK(GenID(-1,1,0)!=0);
  CHECK(GenID(-1,1,0)!=-1);
  CHECK(GenID(-1,1,1)!=0);
  CHECK(GenID(-1,1,1)!=-1);
}

TEST(zeroParentAndMaxChilds) {
  CHECK(GenID(0,0,0)!=0);
  CHECK(GenID(0,0,0)!=-1);
  CHECK(GenID(0,0,1)!=0);
  CHECK(GenID(0,0,1)!=-1);
}

TEST(zeroMaxChilds) {
  CHECK(GenID(1,0,0)!=0);
  CHECK(GenID(1,0,0)!=-1);
  CHECK(GenID(1,0,1)!=0);
  CHECK(GenID(1,0,1)!=-1);
  CHECK(GenID(1,0,2)!=0);
  CHECK(GenID(1,0,2)!=-1);
}

TEST(zeroChild) {
  CHECK(GenID(1,1,0)!=0);
  CHECK(GenID(1,1,0)!=-1);
  CHECK(GenID(1,2,0)!=0);
  CHECK(GenID(1,2,0)!=-1);
  CHECK(GenID(2,1,0)!=0);
  CHECK(GenID(2,1,0)!=-1);
  CHECK(GenID(2,2,0)!=0);
  CHECK(GenID(2,2,0)!=-1);
}

TEST(bruteforce) {
  for(int32_t parent(-256);parent<=256;++parent)
    for(int32_t maxChilds(1);maxChilds<=256;++maxChilds)
      for(int32_t child(0);child<maxChilds;++child) {
        int32_t id(GenID(parent,maxChilds,child));
        CHECK(id!=0);
        CHECK(id!=-1);
      }
}

#if 0 // works, but is a bit slow
TEST(bruteforceWrongInput) {
  for(int32_t parent(-256);parent<=256;++parent) {
    for(int32_t maxChilds(-256);maxChilds<=0;++maxChilds)
      for(int32_t child(-256);child<=256;++child) {
        int32_t id(GenID(parent,maxChilds,child));
        CHECK(id!=0);
        CHECK(id!=-1);
      }
    for(int32_t maxChilds(1);maxChilds<=256;++maxChilds) {
      for(int32_t child(-256);child<=-1;++child) {
        int32_t id(GenID(parent,maxChilds,child));
        CHECK(id!=0);
        CHECK(id!=-1);
      }
      for(int32_t child(maxChilds);child<=256;++child) {
        int32_t id(GenID(parent,maxChilds,child));
        CHECK(id!=0);
        CHECK(id!=-1);
      }
    }
  }
}
#endif

#if 0 // works but is too slow
TEST(bruteforceUniqueness) {
  std::set<int32_t> ids;
  for(int32_t maxChilds(1);maxChilds<=256;++maxChilds) {
    ids.clear();
    for(int32_t parent(-256);parent<=256;++parent) {
      for(int32_t child(0);child<maxChilds;++child) {
        int32_t id(GenID(parent,maxChilds,child));
        auto insertresult(ids.insert(id));
        CHECK(insertresult.second);
        if(!insertresult.second) {
          std::cout << parent << " " << maxChilds << " " << child << ":" << id << std::endl;
        }
      }
    }
  }
}
#endif

#if 0 // only needed in case of user error and really slow
TEST(bruteforceUniquenessWrongInput) {
  std::map<int32_t,int32_t> ids;
  for(int32_t maxChilds(-16);maxChilds<=0;++maxChilds) {
    ids.clear(); // todo: comment this line out to be more strict
    for(int32_t parent(-256);parent<=256;++parent) {
      for(int32_t child(0);child<3;++child) {
        int32_t id(GenID(parent,maxChilds,child));
        auto findresult(ids.find(id));
        if(findresult==ids.end())
          ids.insert(std::make_pair(id,1));
        else {
          ++(findresult->second);
          CHECK(findresult->second<4); // todo: be more strict
          if(findresult->second>=4)
            std::cout << parent << " " << maxChilds << " " << child << ":" << id << "*" << findresult->second << "\n";
        }
      }
    }
  }
}
#endif

}

}
