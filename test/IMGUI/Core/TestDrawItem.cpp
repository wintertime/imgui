#include "IMGUI/Core/DrawItem.hpp"
#include "UnitTest++.h"

namespace IMGUI {

SUITE(DrawItem) {

TEST(create) {
  CHECK((DrawItem(),true));
  CHECK((DrawItem(IRect(),glm::u8vec4(),DrawItem::nothing),true));
}

TEST(getEmptyRect) {
  DrawItem di;
  IRect ir(di.GetRect());
  CHECK_EQUAL(0,ir.pos.x);
  CHECK_EQUAL(0,ir.pos.y);
  CHECK_EQUAL(0,ir.size.x);
  CHECK_EQUAL(0,ir.size.y);
}

TEST(getEmptyColor) {
  DrawItem di;
  glm::u8vec4 c(di.GetColor());
  CHECK_EQUAL(0,c.r);
  CHECK_EQUAL(0,c.g);
  CHECK_EQUAL(0,c.b);
  CHECK_EQUAL(0,c.a);
}

TEST(getEmptyType) {
  DrawItem di;
  CHECK_EQUAL(DrawItem::nothing,di.GetType());
}

TEST(getEmptyString) {
  DrawItem di;
  std::string s(di.GetStr());
  CHECK(s.empty());
}

TEST(getDefaultRect) {
  DrawItem di(IRect(),glm::u8vec4(),DrawItem::nothing);
  IRect ir(di.GetRect());
  CHECK_EQUAL(0,ir.pos.x);
  CHECK_EQUAL(0,ir.pos.y);
  CHECK_EQUAL(0,ir.size.x);
  CHECK_EQUAL(0,ir.size.y);
}

TEST(getDefaultColor) {
  DrawItem di(IRect(),glm::u8vec4(),DrawItem::nothing);
  glm::u8vec4 c(di.GetColor());
  CHECK_EQUAL(0,c.r);
  CHECK_EQUAL(0,c.g);
  CHECK_EQUAL(0,c.b);
  CHECK_EQUAL(0,c.a);
}

TEST(getDefaultType) {
  DrawItem di(IRect(),glm::u8vec4(),DrawItem::nothing);
  CHECK_EQUAL(DrawItem::nothing,di.GetType());
}

TEST(getDefaultString) {
  DrawItem di(IRect(),glm::u8vec4(),DrawItem::nothing);
  std::string s(di.GetStr());
  CHECK(s.empty());
}

TEST(getConstructedRect) {
  DrawItem di(IRect{glm::ivec2(10,11),glm::ivec2(12,13)},glm::u8vec4(),DrawItem::nothing);
  IRect ir(di.GetRect());
  CHECK_EQUAL(10,ir.pos.x);
  CHECK_EQUAL(11,ir.pos.y);
  CHECK_EQUAL(12,ir.size.x);
  CHECK_EQUAL(13,ir.size.y);
}

TEST(getConstructedColor) {
  DrawItem di(IRect(),glm::u8vec4(73,84,95,106),DrawItem::nothing);
  glm::u8vec4 c(di.GetColor());
  CHECK_EQUAL(73,c.r);
  CHECK_EQUAL(84,c.g);
  CHECK_EQUAL(95,c.b);
  CHECK_EQUAL(106,c.a);
}

TEST(getConstructedType) {
  DrawItem di(IRect(),glm::u8vec4(),DrawItem::texture);
  CHECK_EQUAL(DrawItem::texture,di.GetType());
}

TEST(getConstructedString) {
  DrawItem di(IRect(),glm::u8vec4(),DrawItem::nothing,"abcdef");
  std::string s(di.GetStr());
  CHECK_EQUAL(6u,s.length());
  CHECK_EQUAL(0,s.compare("abcdef"));
}

TEST(getCopyConstructedRect) {
  DrawItem di(IRect{glm::ivec2(10,11),glm::ivec2(12,13)},glm::u8vec4(),DrawItem::nothing);
  DrawItem di2(di);
  IRect ir(di2.GetRect());
  CHECK_EQUAL(10,ir.pos.x);
  CHECK_EQUAL(11,ir.pos.y);
  CHECK_EQUAL(12,ir.size.x);
  CHECK_EQUAL(13,ir.size.y);
}

TEST(getCopyConstructedColor) {
  DrawItem di(IRect(),glm::u8vec4(73,84,95,106),DrawItem::nothing);
  DrawItem di2(di);
  glm::u8vec4 c(di2.GetColor());
  CHECK_EQUAL(73,c.r);
  CHECK_EQUAL(84,c.g);
  CHECK_EQUAL(95,c.b);
  CHECK_EQUAL(106,c.a);
}

TEST(getCopyConstructedType) {
  DrawItem di(IRect(),glm::u8vec4(),DrawItem::texture);
  DrawItem di2(di);
  CHECK_EQUAL(DrawItem::texture,di2.GetType());
}

TEST(getCopyConstructedString) {
  DrawItem di(IRect(),glm::u8vec4(),DrawItem::nothing,"abcdef");
  DrawItem di2(di);
  std::string s(di2.GetStr());
  CHECK_EQUAL(6u,s.length());
  CHECK_EQUAL(0,s.compare("abcdef"));
}

TEST(getCopyRect) {
  DrawItem di(IRect{glm::ivec2(10,11),glm::ivec2(12,13)},glm::u8vec4(),DrawItem::nothing);
  DrawItem di2;
  di2=di;
  IRect ir(di2.GetRect());
  CHECK_EQUAL(10,ir.pos.x);
  CHECK_EQUAL(11,ir.pos.y);
  CHECK_EQUAL(12,ir.size.x);
  CHECK_EQUAL(13,ir.size.y);
}

TEST(getCopyColor) {
  DrawItem di(IRect(),glm::u8vec4(73,84,95,106),DrawItem::nothing);
  DrawItem di2;
  di2=di;
  glm::u8vec4 c(di2.GetColor());
  CHECK_EQUAL(73,c.r);
  CHECK_EQUAL(84,c.g);
  CHECK_EQUAL(95,c.b);
  CHECK_EQUAL(106,c.a);
}

TEST(getCopyType) {
  DrawItem di(IRect(),glm::u8vec4(),DrawItem::texture);
  DrawItem di2;
  di2=di;
  CHECK_EQUAL(DrawItem::texture,di2.GetType());
}

TEST(getCopyString) {
  DrawItem di(IRect(),glm::u8vec4(),DrawItem::nothing,"abcdef");
  DrawItem di2;
  di2=di;
  std::string s(di2.GetStr());
  CHECK_EQUAL(6u,s.length());
  CHECK_EQUAL(0,s.compare("abcdef"));
}

TEST(getMoveConstructedRect) {
  DrawItem di(IRect{glm::ivec2(10,11),glm::ivec2(12,13)},glm::u8vec4(),DrawItem::nothing);
  DrawItem di2(std::move(di));
  IRect ir(di2.GetRect());
  CHECK_EQUAL(10,ir.pos.x);
  CHECK_EQUAL(11,ir.pos.y);
  CHECK_EQUAL(12,ir.size.x);
  CHECK_EQUAL(13,ir.size.y);
}

TEST(getMoveConstructedColor) {
  DrawItem di(IRect(),glm::u8vec4(73,84,95,106),DrawItem::nothing);
  DrawItem di2(std::move(di));
  glm::u8vec4 c(di2.GetColor());
  CHECK_EQUAL(73,c.r);
  CHECK_EQUAL(84,c.g);
  CHECK_EQUAL(95,c.b);
  CHECK_EQUAL(106,c.a);
}

TEST(getMoveConstructedType) {
  DrawItem di(IRect(),glm::u8vec4(),DrawItem::texture);
  DrawItem di2(std::move(di));
  CHECK_EQUAL(DrawItem::texture,di2.GetType());
}

TEST(getMoveConstructedString) {
  DrawItem di(IRect(),glm::u8vec4(),DrawItem::nothing,"abcdef");
  DrawItem di2(std::move(di));
  std::string s(di2.GetStr());
  CHECK_EQUAL(6u,s.length());
  CHECK_EQUAL(0,s.compare("abcdef"));
}

TEST(getMoveCopyRect) {
  DrawItem di(IRect{glm::ivec2(10,11),glm::ivec2(12,13)},glm::u8vec4(),DrawItem::nothing);
  DrawItem di2;
  di2=std::move(di);
  IRect ir(di2.GetRect());
  CHECK_EQUAL(10,ir.pos.x);
  CHECK_EQUAL(11,ir.pos.y);
  CHECK_EQUAL(12,ir.size.x);
  CHECK_EQUAL(13,ir.size.y);
}

TEST(getMoveCopyColor) {
  DrawItem di(IRect(),glm::u8vec4(73,84,95,106),DrawItem::nothing);
  DrawItem di2;
  di2=std::move(di);
  glm::u8vec4 c(di2.GetColor());
  CHECK_EQUAL(73,c.r);
  CHECK_EQUAL(84,c.g);
  CHECK_EQUAL(95,c.b);
  CHECK_EQUAL(106,c.a);
}

TEST(getMoveCopyType) {
  DrawItem di(IRect(),glm::u8vec4(),DrawItem::texture);
  DrawItem di2;
  di2=std::move(di);
  CHECK_EQUAL(DrawItem::texture,di2.GetType());
}

TEST(getMoveCopyString) {
  DrawItem di(IRect(),glm::u8vec4(),DrawItem::nothing,"abcdef");
  DrawItem di2;
  di2=std::move(di);
  std::string s(di2.GetStr());
  CHECK_EQUAL(6u,s.length());
  CHECK_EQUAL(0,s.compare("abcdef"));
}

}

}
