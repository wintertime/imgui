#include "TestSetupFont.hpp"
#include <stdexcept>
#include "UnitTest++.h"

const sf::Font* testFont=0;

int main(int argc,char* argv[]) {
  sf::Font font;
  if(!font.loadFromFile("C:/Windows/Fonts/arial.ttf"))
    throw std::runtime_error("error loading font");
  testFont=&font;
  return UnitTest::RunAllTests();
}
