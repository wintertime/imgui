#include "IMGUI/Renderer/CachedIMOGLRenderer.hpp"
#include "IMGUI/Extension/SFML/SFMLTextureCache.hpp"
#include "IMGUI/Extension/SFML/SFMLLoadTexture.hpp"
#include "IMGUI/Extension/SFML/SFMLMakeTextureFromText.hpp"
#include "TestSetupFont.hpp"
#include "UnitTest++.h"

namespace IMGUI {

SUITE(CachedIMOGLRenderer) {

TEST(create) {
  SFMLTextureCache<std::shared_ptr<sf::Texture> (*)(const std::string& id)> textures{SFMLLoadTexture};
  SFMLTextureCache<SFMLMakeTextureFromText> textTextures{SFMLMakeTextureFromText(*testFont)};
  CHECK((CachedIMOGLRenderer(IRect(),textures,textTextures),true));
}

TEST(SetScreenRect) {
  IRect rect;
  SFMLTextureCache<std::shared_ptr<sf::Texture> (*)(const std::string& id)> textures{SFMLLoadTexture};
  SFMLTextureCache<SFMLMakeTextureFromText> textTextures{SFMLMakeTextureFromText(*testFont)};
  CachedIMOGLRenderer rr(rect,textures,textTextures);
  CHECK((rr.SetScreenRect(rect),true));
}

// todo: more tests would need OpenGL running

}

}
