#ifndef TEST_IMGUI_TESTSETUPFONT_HPP
#define TEST_IMGUI_TESTSETUPFONT_HPP

#include "SFML/Graphics/Font.hpp"

extern const sf::Font* testFont;

#endif // TEST_IMGUI_TESTSETUPFONT_HPP
