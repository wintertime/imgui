#include "IMGUI/Renderer/IMOGLRenderer.hpp"
#include "IMGUI/Extension/SFML/SFMLTextureCache.hpp"
#include "IMGUI/Extension/SFML/SFMLLoadTexture.hpp"
#include "IMGUI/Extension/SFML/SFMLMakeTextureFromText.hpp"
#include "TestSetupFont.hpp"
#include "UnitTest++.h"

namespace IMGUI {

SUITE(IMOGLRenderer) {

TEST(create) {
  SFMLTextureCache<std::shared_ptr<sf::Texture> (*)(const std::string& id)> textures{SFMLLoadTexture};
  SFMLTextureCache<SFMLMakeTextureFromText> textTextures{SFMLMakeTextureFromText(*testFont)};
  CHECK((IMOGLRenderer(IRect(),textures,textTextures),true));
}

TEST(SetScreenRect) {
  IRect rect;
  SFMLTextureCache<std::shared_ptr<sf::Texture> (*)(const std::string& id)> textures{SFMLLoadTexture};
  SFMLTextureCache<SFMLMakeTextureFromText> textTextures{SFMLMakeTextureFromText(*testFont)};
  IMOGLRenderer rr(rect,textures,textTextures);
  CHECK((rr.SetScreenRect(rect),true));
}

// todo: more tests would need OpenGL running

}

}
