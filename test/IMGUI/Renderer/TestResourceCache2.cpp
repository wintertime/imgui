#include "IMGUI/Renderer/ResourceCache2.hpp"

#include "UnitTest++.h"

namespace IMGUI {

SUITE(ResourceCache2) {

static int num=0;
int MakeR(const std::string& id) {
  if(id.empty())
    throw std::invalid_argument("empty id");
  if(id=="R")
    return -1;
  return num++;
}

TEST(empty) {
  ResourceCache2<int,int (*)(const std::string&)> rc(MakeR);
  CHECK_THROW(rc[""],std::invalid_argument);
  CHECK(!rc.IsCached(""));
}

TEST(Caching) {
  ResourceCache2<int,int (*)(const std::string&)> rc(MakeR);
  CHECK(!rc.IsCached("R"));
  CHECK_EQUAL(rc["R"],-1);
  CHECK(rc.IsCached("R"));
  CHECK_EQUAL(rc["x"],0);
  CHECK(rc.IsCached("x"));
  CHECK_EQUAL(rc["R"],-1);
  CHECK(rc.IsCached("R"));
  CHECK_EQUAL(rc["x"],0);
  CHECK(rc.IsCached("x"));
  CHECK(rc.Forget("R"));
  CHECK(!rc.Forget("R"));
  CHECK(!rc.IsCached("R"));
}

TEST(Clear) {
  ResourceCache2<int,int (*)(const std::string&)> rc(MakeR);
  CHECK_EQUAL(rc["R"],-1);
  CHECK(rc.IsCached("R"));
  rc.Clear();
  CHECK(!rc.IsCached("R"));
  CHECK_EQUAL(rc["R"],-1);
}

}

}
