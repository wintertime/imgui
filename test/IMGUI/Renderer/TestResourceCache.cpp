#include "IMGUI/Renderer/ResourceCache.hpp"

#include "UnitTest++.h"

namespace IMGUI {

SUITE(ResourceCache) {

static int num=0;
int MakeR(const std::string& id) {
  if(id.empty())
    throw std::invalid_argument("empty id");
  if(id=="R")
    return -1;
  return num++;
}

TEST(empty) {
  ResourceCache<int,MakeR> rc;
  CHECK_THROW(rc[""],std::invalid_argument);
  CHECK(!rc.IsCached(""));
}

TEST(Caching) {
  ResourceCache<int,MakeR> rc;
  CHECK(!rc.IsCached("R"));
  CHECK_EQUAL(rc["R"],-1);
  CHECK(rc.IsCached("R"));
  CHECK_EQUAL(rc["x"],0);
  CHECK(rc.IsCached("x"));
  CHECK_EQUAL(rc["R"],-1);
  CHECK(rc.IsCached("R"));
  CHECK_EQUAL(rc["x"],0);
  CHECK(rc.IsCached("x"));
  CHECK(rc.Forget("R"));
  CHECK(!rc.Forget("R"));
  CHECK(!rc.IsCached("R"));
}

TEST(Clear) {
  ResourceCache<int,MakeR> rc;
  CHECK_EQUAL(rc["R"],-1);
  CHECK(rc.IsCached("R"));
  rc.Clear();
  CHECK(!rc.IsCached("R"));
  CHECK_EQUAL(rc["R"],-1);
}

}

}
