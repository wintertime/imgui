#include "Game.hpp"
#include "../../Common/GameGUI.hpp"
#include "IMGUI/Extension/SFML/SFMLLoadTexture.hpp"
#include "IMGUI/Core/Frame.hpp"
#include "IMGUI/Core/Event.hpp"
#include "IMGUI/Core/Style.hpp"
#include "SFML/Graphics.hpp"

Game::Game(const sf::Font& font_) :
  windowsize(1024,768),
  fullscreen(0),
  depth(32),
  stencil(0),
  antialias(0),
  vsync(1),
  waitevent(1),
  gamestate(0),
  font(font_),
  gameWindow(nullptr),
  guiTexture(nullptr),
  guiRenderer(nullptr),
  gui()
{
}

Game::~Game() {
}

int Game::Run() {
  sf::RenderWindow window(sf::VideoMode(windowsize.x,windowsize.y),"Game",
                    fullscreen ? sf::Style::Fullscreen : sf::Style::Default,
                    sf::ContextSettings(depth,stencil,antialias,1,1));
  window.setVerticalSyncEnabled(vsync);
  gameWindow=&window;
  sf::RenderTexture renderTexture;
  if(!renderTexture.create(windowsize.x,windowsize.y))
    return 1;
  guiTexture=&renderTexture;
  IMGUI::SFMLTextureCache<std::shared_ptr<sf::Texture> (*)(const std::string& id)> textures{IMGUI::SFMLLoadTexture};
  IMGUI::SFMLRenderer sfmlRenderer(renderTexture,font,textures);
  guiRenderer=&sfmlRenderer;

  sf::Event event;
  // make sure the GUI functions are run at least once at start and after an event
  bool needRunGUI(true);
  // main loop
  while(window.isOpen()) {
    Update();
    window.setActive();
    window.clear();
    Draw();
    window.display();

    bool gotEvent=false;
    // let the GUI functions run one more time on next frame to update graphics after processing an event
    if(!needRunGUI && waitevent && window.waitEvent(event)) {
      gotEvent=true;
      IMGUI::Event guiEvent(event);
      if(ProcessEvent(guiEvent) || event.type==sf::Event::Closed)
        window.close();
    }
    while(window.pollEvent(event)) {
      gotEvent=true;
      IMGUI::Event guiEvent(event);
      if(ProcessEvent(guiEvent) || event.type==sf::Event::Closed)
        window.close();
    }
    if(!gotEvent) {
      // create null Event to redraw the GUI
      IMGUI::Event guiEvent;
      if(ProcessEvent(guiEvent))
        window.close();
    }
    needRunGUI=gotEvent;
  }
  gameWindow=nullptr;
  guiTexture=nullptr;
  guiRenderer=nullptr;
  return 0;
}

bool Game::ProcessEvent(IMGUI::Event& event) {
  if(event.type==IMGUI::Event::ResizeW) {
    windowsize=event.point;
    if(!guiTexture->create(windowsize.x,windowsize.y))
      return true;
  }
  guiTexture->setActive();
  guiTexture->clear(sf::Color::Transparent);
  IMGUI::Frame frame(gui,*guiRenderer,event);
  IMGUI::Style style;
  IMGUI::IRect space{glm::ivec2(0,0),windowsize};
  bool rv(GameGUI(1,frame,style,space,IMGUI::none,gamestate));
  guiTexture->display();
  return rv;
}

void Game::Update() {
  // todo
}

void Game::Draw() {
  // todo: draw something
  sf::Vertex vertices[] = {
    sf::Vertex(sf::Vector2f(0.f         ,0.f         ),sf::Vector2f(0.f         ,0.f         )),
    sf::Vertex(sf::Vector2f(0.f         ,windowsize.y),sf::Vector2f(0.f         ,windowsize.y)),
    sf::Vertex(sf::Vector2f(windowsize.x,windowsize.y),sf::Vector2f(windowsize.x,windowsize.y)),
    sf::Vertex(sf::Vector2f(windowsize.x,windowsize.y),sf::Vector2f(windowsize.x,windowsize.y)),
    sf::Vertex(sf::Vector2f(windowsize.x,0.f         ),sf::Vector2f(windowsize.x,0.f         )),
    sf::Vertex(sf::Vector2f(0.f         ,0.f         ),sf::Vector2f(0.f         ,0.f         ))
  };
  gameWindow->draw(vertices,6,sf::Triangles,&guiTexture->getTexture());
}
