#ifndef EXAMPLE_IMGUI_RENDERTEXTURE_UNCACHED_GAME_HPP
#define EXAMPLE_IMGUI_RENDERTEXTURE_UNCACHED_GAME_HPP

#include "IMGUI/Extension/SFML/SFMLRenderer.hpp"
#include "IMGUI/Core/GUI.hpp"
#include "glm/glm.hpp"

namespace sf {

class RenderWindow;
class RenderTexture;

}

namespace IMGUI {

class Event;

}

class Game {
  glm::ivec2 windowsize;
  int32_t fullscreen;
  int32_t depth;
  int32_t stencil;
  int32_t antialias;
  int32_t vsync;
  int32_t waitevent;

  int32_t gamestate;

  const sf::Font& font;
  sf::RenderWindow* gameWindow;
  sf::RenderTexture* guiTexture;
  IMGUI::SFMLRenderer* guiRenderer;

  IMGUI::GUI gui;

public:
  explicit Game(const sf::Font& font_);
  virtual ~Game();
  virtual int Run();
protected:
  virtual bool ProcessEvent(IMGUI::Event& event);
  virtual void Update();
  virtual void Draw();

private:
  Game(const Game& game);
  const Game& operator=(const Game& game);
};

#endif // EXAMPLE_IMGUI_RENDERTEXTURE_UNCACHED_GAME_HPP
