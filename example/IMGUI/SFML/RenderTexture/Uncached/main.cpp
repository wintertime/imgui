#include "Game.hpp"
#include "SFML/Graphics/Font.hpp"
#include <stdexcept>

int main(int argc,char* argv[]) {
  sf::Font font;
  bool fontFound(false);
  for(int i(1);i<argc;++i)
    if(font.loadFromFile(argv[i])) {
      fontFound=true;
      break;
    }
  if(!fontFound && !font.loadFromFile("C:/Windows/Fonts/arial.ttf"))
    throw std::runtime_error("error loading font");
  Game game(font);
  return game.Run();
}
