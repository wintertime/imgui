#include "GameGUI.hpp"
#include "IMGUI/Widgets/Widgets.hpp"
#include "IMGUI/Core/WidgetLogic.hpp"
#include "IMGUI/Core/Frame.hpp"
#include "IMGUI/Core/Event.hpp"
#include "IMGUI/Core/Style.hpp"
#include "IMGUI/Core/GenID.hpp"
#include "SFML/Window/Keyboard.hpp"

bool Intro(int32_t id,IMGUI::Frame& frame,const IMGUI::Style& style,IMGUI::IRect& space,IMGUI::BorderConnect /*bc*/) {
  // todo: draw something better looking
  IMGUI::Style style2(style);
  style2.background=glm::u8vec4(0,0,0,0);
  glm::ivec2 size(space.size.x/2,space.size.y/4);
  IMGUI::IRect space2(space);
  IMGUI::TextBox(frame,style2,space2,IMGUI::center,size,"Intro");
  return (ButtonLogic(id,frame,space) || frame.event.type==IMGUI::Event::KeyUp);
}

bool GameOver(int32_t id,IMGUI::Frame& frame,const IMGUI::Style& style,IMGUI::IRect& space,IMGUI::BorderConnect /*bc*/) {
  // todo: draw something better looking
  IMGUI::Style style2(style);
  style2.background=glm::u8vec4(0,0,0,0);
  glm::ivec2 size((space.size.x*3)/4,space.size.y/4);
  IMGUI::IRect space2(space);
  IMGUI::TextBox(frame,style2,space2,IMGUI::center,size,"Game Over");
  return (ButtonLogic(id,frame,space) || frame.event.type==IMGUI::Event::KeyUp);
}

int32_t MainMenu(int32_t id,IMGUI::Frame& frame,IMGUI::Style& style,IMGUI::IRect& space,IMGUI::BorderConnect bc) {
  // todo: do and draw more and better looking things
  IMGUI::Style style2(style);
  style2.background.a=127;
  style2.foreground.a=127;
  glm::ivec2 size(space.size.x/2,space.size.y/2);
  IMGUI::IRect space2(space);
  std::vector<std::string> texts;
  texts.push_back("Quit Game");
  texts.push_back("Continue Game");
  texts.push_back("New Game");
  return TextButtonSet(IMGUI::GenID(id,2,0),frame,style2,space2,bc,size,texts);
}

int32_t GameHUD(int32_t id,IMGUI::Frame& frame,IMGUI::Style& style,IMGUI::IRect& space,IMGUI::BorderConnect /*bc*/) {
  IMGUI::Style style2(style);
  style2.background.a=127;
  style2.foreground.a=127;
  // todo: do and draw something more useful
  glm::ivec2 size((space.size.x+space.size.y)/32,(space.size.x+space.size.y)/32);
  IMGUI::IRect space2(space);
  if(TextButton(IMGUI::GenID(id,2,0),frame,style2,space2,IMGUI::upright,size,"X"))
    return 4;
  if(frame.event.type==IMGUI::Event::KeyUp) {
    switch(frame.event.data) {
    case sf::Keyboard::F4:
      return 1;
    case sf::Keyboard::F3:
      return 3;
    case sf::Keyboard::F2:
      return 2;
    case sf::Keyboard::F1:
      return 4;
    case sf::Keyboard::Escape:
      return 2;
    default:
      ;
    }
  }
  return 0;
}

int32_t GameInput(int32_t /*id*/,IMGUI::Frame& /*frame*/,IMGUI::Style& /*style*/,IMGUI::IRect& /*space*/,IMGUI::BorderConnect /*bc*/,int32_t infoFromHUD) {
  if(infoFromHUD>=0)
    return infoFromHUD;
  // todo: channel the input to game entities
  return 0;
}

int32_t GameWindow(int32_t id,IMGUI::Frame& frame,IMGUI::Style& style,IMGUI::IRect& space,IMGUI::BorderConnect bc) {
  int32_t rv(GameHUD(IMGUI::GenID(id,2,0),frame,style,space,bc));
  return GameInput(IMGUI::GenID(id,2,1),frame,style,space,bc,rv);
}

int32_t GameGUI(int32_t id,IMGUI::Frame& frame,IMGUI::Style& style,IMGUI::IRect& space,IMGUI::BorderConnect bc,int32_t& gamestate) {
  if(gamestate==0) {
    if(Intro(id+1,frame,style,space,bc)) {
      // intro ended, show main menu
      gamestate=0x01;
    }
  } else if(gamestate&0x01) {
    switch(MainMenu(id+2,frame,style,space,bc)) {
    case 1: // quit
      return 1;
    case 3: // start game
      gamestate=0x02;
      break;
    case 2: // return to game
      gamestate|=0x02;
      gamestate&=~0x01;
    default: // continue showing same
      ;
    }
  } else if(gamestate&0x02) {
    switch(GameWindow(id+3,frame,style,space,bc)) {
    case 1: // quit
      return 1;
    case 3: // quit to main menu
      gamestate=0x01;
      break;
    case 2: // show main menu
      gamestate|=0x01;
      break;
    case 4: // show game over
      gamestate=0x04;
    default:
      ;
    }
  } else {
    if(GameOver(id+4,frame,style,space,bc)) {
      // outro ended, show main menu
      gamestate=0x01;
    }
  }
  return 0;
}
