#ifndef IMGUI_FAKETEXTURECACHE_HPP
#define IMGUI_FAKETEXTURECACHE_HPP

#include "IMGUI/Renderer/TextureCacheBase.hpp"

namespace IMGUI {

/** FakeTextureCache is an empty implementation of TextureCacheBase
*/
class FakeTextureCache : public TextureCacheBase {
public:
  explicit FakeTextureCache() {
  }
  ~FakeTextureCache() {
  }
  void Clear() {
  }
  void Update(const std::string& /*id*/) {
  }
  void Bind(const std::string& /*id*/) {
  }
  void UnBind() const {
  }
};

}

#endif // IMGUI_FAKETEXTURECACHE_HPP
