#ifndef EXAMPLE_IMGUI_GAMEGUI_HPP
#define EXAMPLE_IMGUI_GAMEGUI_HPP

#include "IMGUI/Core/GUIFwd.hpp"

/** GameGUI is the top level widget implementing some basic state management
    for switching between different screens, that is used by all examples.
*/
int32_t GameGUI(int32_t id,IMGUI::Frame& frame,IMGUI::Style& style,IMGUI::IRect& space,IMGUI::BorderConnect bc,int32_t& gamestate);

#endif // EXAMPLE_IMGUI_GAMEGUI_HPP
