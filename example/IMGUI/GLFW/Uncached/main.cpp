#include "Game.hpp"
#include "IMGUI/Extension/LoadFromFile.hpp"

int main(int argc,char* argv[]) {
  size_t fontSize=0;
  std::unique_ptr<char[]> font;
  for(int i(1);i<argc;++i) {
    font=IMGUI::LoadFromFile(argv[i],fontSize);
    if(font)
      break;
  }
  if(!font) {
    font=IMGUI::LoadFromFile("C:/Windows/Fonts/arial.ttf",fontSize);
    if(!font)
      throw std::runtime_error("error loading font");
  }
  Game game(font.get());
  return game.Run();
}
