#include "Game.hpp"
#include "../Common/GameGUI.hpp"
#include "IMGUI/Extension/GLFW/GLFWEventQueue.hpp"
#include "IMGUI/Extension/STB/STBLoadTexture.hpp"
#include "IMGUI/Extension/STB/STBMakeTextureFromText.hpp"
#include "IMGUI/Renderer/IMOGLTextureCache.hpp"
#include "IMGUI/Core/Frame.hpp"
#include "IMGUI/Core/Style.hpp"
#include "GLFW/glfw3.h"
#include <iostream>
#include <stdexcept>

namespace {

void ErrorCallback(int error,const char* description) {
  std::cerr << "Error " << std::hex << error << "\n" << description << std::endl;
}

}

Game::Game(const char* font_) :
  windowsize(1024,768),
  fullscreen(0),
  depth(32),
  stencil(0),
  vsync(1),
  waitevent(1),
  gamestate(0),
  textures(new IMGUI::IMOGLTextureCache<IMGUI::IMOGLTexture (*)(const std::string& id)>(IMGUI::STBLoadTexture)),
  textTextures(new IMGUI::IMOGLTextureCache<IMGUI::STBMakeTextureFromText>(IMGUI::STBMakeTextureFromText(font_))),
  guiRenderer(IMGUI::IRect{glm::ivec2(0,0),windowsize},*textures,*textTextures),
  gui()
{
  glfwSetErrorCallback(ErrorCallback);
  if(!glfwInit())
    throw std::runtime_error("Error on GLFW init");
}

Game::~Game() {
  glfwTerminate();
}

int Game::Run() {
  glfwWindowHint(GLFW_DEPTH_BITS,depth);
  glfwWindowHint(GLFW_STENCIL_BITS,stencil);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,1);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,1);
  GLFWwindow* window(glfwCreateWindow(windowsize.x,windowsize.y,"Game",
                    fullscreen ? glfwGetPrimaryMonitor() : nullptr,
                    nullptr));
  if(!window)
    throw std::runtime_error("Error on window creation");
  glfwMakeContextCurrent(window);
  glfwSwapInterval(vsync);
  {
    int fbwidth;
    int fbheight;
    glfwGetFramebufferSize(window,&fbwidth,&fbheight);
    glViewport(0,0,fbwidth,fbheight);
  }
  try {
    guiRenderer.SetScreenRect(IMGUI::IRect{glm::ivec2(0,0),windowsize});
    IMGUI::GLFWEventQueue eventQueue(window);

    // make sure the GUI functions are run at least once at start and after an event
    bool needRunGUI(true);
    // main loop
    while(!glfwWindowShouldClose(window)) {
      Update();
      Draw();
      glfwSwapBuffers(window);

      bool gotEvent=false;
      IMGUI::Event event;
      if(waitevent && !needRunGUI)
        eventQueue.WaitEvent();
      while(eventQueue.PollEvent(event)) {
        gotEvent=true;
        if(ProcessEvent(window,event) || event.type==IMGUI::Event::Close)
          glfwSetWindowShouldClose(window,1);
      }
      // let the GUI functions run one more time on next frame to update graphics after processing an event
      if(gotEvent)
        needRunGUI=true;
      else {
        if(needRunGUI) {
          needRunGUI=false;
          // create null Event to redraw the GUI
          IMGUI::Event guiEvent;
          if(ProcessEvent(window,guiEvent))
            glfwSetWindowShouldClose(window,1);
        }
      }
    }
  } catch(...) {
    glfwMakeContextCurrent(nullptr);
    glfwDestroyWindow(window);
    throw;
  }
  glfwMakeContextCurrent(nullptr);
  glfwDestroyWindow(window);
  return 0;
}

bool Game::ProcessEvent(GLFWwindow* window,IMGUI::Event& event) {
  if(event.type==IMGUI::Event::ResizeW) {
    windowsize=event.point;
    guiRenderer.SetScreenRect(IMGUI::IRect{glm::ivec2(0,0),event.point});
  } else if(event.type==IMGUI::Event::ResizeF)
    glViewport(0,0,event.point.x,event.point.y);
  guiRenderer.ClearCache();
  IMGUI::Frame frame(gui,guiRenderer,event);
  IMGUI::Style style;
  IMGUI::IRect space{glm::ivec2(0,0),windowsize};
  return GameGUI(1,frame,style,space,IMGUI::none,gamestate);
}

void Game::Update() {
  // todo
  guiRenderer.UpdateCache();
}

void Game::Draw() {
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
  // todo: draw something
  guiRenderer.RenderCache();
}
