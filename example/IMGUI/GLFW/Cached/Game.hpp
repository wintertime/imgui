#ifndef EXAMPLE_IMGUI_GLFW_CACHED_GAME_HPP
#define EXAMPLE_IMGUI_GLFW_CACHED_GAME_HPP

#include "IMGUI/Renderer/CachedIMOGLRenderer.hpp"
#include "IMGUI/Core/GUI.hpp"
#include "glm/glm.hpp"
#include <memory>

extern "C" {
typedef struct GLFWwindow GLFWwindow;
}

namespace IMGUI {

class TextureCacheBase;
class Event;

}

class Game {
  glm::ivec2 windowsize;
  int32_t fullscreen;
  int32_t depth;
  int32_t stencil;
  int32_t vsync;
  int32_t waitevent;

  int32_t gamestate;

  std::unique_ptr<IMGUI::TextureCacheBase> textures;
  std::unique_ptr<IMGUI::TextureCacheBase> textTextures;
  IMGUI::CachedIMOGLRenderer guiRenderer;

  IMGUI::GUI gui;

public:
  explicit Game(const char* font_);
  virtual ~Game();
  virtual int Run();
protected:
  virtual bool ProcessEvent(GLFWwindow* window,IMGUI::Event& event);
  virtual void Update();
  virtual void Draw();

private:
  Game(const Game& game);
  const Game& operator=(const Game& game);
};

#endif // EXAMPLE_IMGUI_GLFW_CACHED_GAME_HPP
