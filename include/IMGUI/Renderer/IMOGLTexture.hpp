#ifndef IMGUI_IMOGLTEXTURE_HPP
#define IMGUI_IMOGLTEXTURE_HPP

#include "GL/gl.h"

namespace IMGUI {

/** IMOGLTexture wraps an OpenGL Texture.
*/
class IMOGLTexture {
  GLuint texture;
public:
  explicit IMOGLTexture() {
    glGenTextures(1,&texture);
  }

  explicit IMOGLTexture(GLuint texture_) :
    texture(texture_)
  {
  }

  explicit IMOGLTexture(int x,int y,unsigned char* ptr) {
    glGenTextures(1,&texture);
    Init(x,y,ptr);
  }

  IMOGLTexture(IMOGLTexture&& t) :
    texture(t.texture)
  {
    t.texture=0;
  }

  IMOGLTexture& operator=(IMOGLTexture&& t) {
    if(&t!=this) {
      glDeleteTextures(1,&texture);
      texture=t.texture;
      t.texture=0;
    }
    return *this;
  }

  IMOGLTexture(const IMOGLTexture& ) = delete;

  IMOGLTexture& operator=(const IMOGLTexture& ) = delete;

  ~IMOGLTexture() {
    glDeleteTextures(1,&texture);
  }

  void Init(int x,int y,unsigned char* ptr) {
    Bind();
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA8,x,y,0,GL_RGBA,GL_UNSIGNED_BYTE,ptr);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    UnBind();
  }

  void Bind() const {
    glBindTexture(GL_TEXTURE_2D,texture);
  }

  static void UnBind() {
    glBindTexture(GL_TEXTURE_2D,0);
  }

  GLuint Get() {
    return texture;
  }
};

}

#endif // IMGUI_IMOGLTEXTURE_HPP
