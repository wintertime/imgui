#ifndef IMGUI_CACHEDRENDERER_HPP
#define IMGUI_CACHEDRENDERER_HPP

#include "RenderQueue.hpp"
#include "IMGUI/Core/SplitRenderer.hpp"

namespace IMGUI {

/** CachedRenderer is a helper class that implements caching of Draw calls.
    It is not rendering anything by itself.
*/
class CachedRenderer : virtual public SplitRenderer {
  RenderQueue drawCache;

public:
  CachedRenderer();
  ~CachedRenderer();

  void Draw(DrawItem&& drawItem);
  void ClearCache();
  void UpdateCache();
  void RenderCache();
};

}

#endif // IMGUI_CACHEDRENDERER_HPP
