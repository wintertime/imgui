#ifndef IMGUI_RENDERQUEUE_HPP
#define IMGUI_RENDERQUEUE_HPP

#include "IMGUI/Core/DrawItem.hpp"
#include <vector>

namespace IMGUI {

// todo: improve this
/** RenderQueue is only a basic implementation.
*/
class RenderQueue {
  std::vector<DrawItem> items;

public:

  void Add(DrawItem&& drawItem) {
    items.push_back(std::move(drawItem));
  }

  void Clear() {
    items.clear();
  }

  template<class Renderer> void Update(Renderer& renderer) const {
    for(auto i(items.cbegin()),e(items.cend());i!=e;++i)
      renderer.Update(*i);
  }

  template<class Renderer> void Draw(Renderer& renderer) const {
    renderer.BeginRender();
    for(auto i(items.cbegin()),e(items.cend());i!=e;++i)
      renderer.Render(*i);
    renderer.EndRender();
  }
};

}

#endif // IMGUI_RENDERQUEUE_HPP
