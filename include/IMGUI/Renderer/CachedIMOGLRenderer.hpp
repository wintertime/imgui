#ifndef IMGUI_CACHEDIMOGLRENDERER_HPP
#define IMGUI_CACHEDIMOGLRENDERER_HPP

#include "IMOGLRenderer.hpp"
#include "CachedRenderer.hpp"

namespace IMGUI {

/** CachedIMOGLRenderer is a slightly better version of IMOGLRenderer.
    Its easier to fit into the game cause of the added caching of Draw calls,
    but still suffers from the prerendered text.
*/
class CachedIMOGLRenderer : public IMOGLRenderer, public CachedRenderer {
public:
  CachedIMOGLRenderer(const IRect& screenRect_,TextureCacheBase& textures_,TextureCacheBase& textTextures_);
  ~CachedIMOGLRenderer();
};

}

#endif // IMGUI_CACHEDIMOGLRENDERER_HPP
