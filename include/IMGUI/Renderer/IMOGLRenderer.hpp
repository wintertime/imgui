#ifndef IMGUI_IMOGLRENDERER_HPP
#define IMGUI_IMOGLRENDERER_HPP

#include "IMGUI/Core/SplitRenderer.hpp"
#include "IMGUI/Core/IRect.hpp"

namespace IMGUI {

class TextureCacheBase;

/** IMOGLRenderer is a crude, old-fashioned, proof-of-concept Renderer.
    It caches all text as prerendered Textures that will fill up memory,
    while the resizing of a fixed text size results in suboptimally looking
    text output.
    Its here to have something not directly dependent on SFML and for the
    example that uses sf::Window (where SFML graphics can not be used on).
    You better avoid using it; especially in combination with the EditBox
    it will end badly.
*/
class IMOGLRenderer : virtual public SplitRenderer {
  IRect screenRect;
  TextureCacheBase& textures;
  TextureCacheBase& textTextures;

public:
  IMOGLRenderer(const IRect& screenRect_,TextureCacheBase& textures_,TextureCacheBase& textTextures_);
  ~IMOGLRenderer();

  void SetScreenRect(const IRect& screenRect_);

  void Update(const DrawItem& drawItem);
  void BeginRender();
  void Render(const DrawItem& drawItem);
  void EndRender();

private:
  IMOGLRenderer(const IMOGLRenderer& other);
  const IMOGLRenderer& operator=(const IMOGLRenderer& other);
};

}

#endif // IMGUI_IMOGLRENDERER_HPP
