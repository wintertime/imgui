#ifndef IMGUI_IMOGLTEXTURECACHE_HPP
#define IMGUI_IMOGLTEXTURECACHE_HPP

#include "IMGUI/Renderer/TextureCacheBase.hpp"
#include "IMGUI/Renderer/ResourceCache2.hpp"
#include "IMGUI/Renderer/IMOGLTexture.hpp"
#include <memory>

namespace IMGUI {

/** IMOGLTextureCache provides an implementation of TextureCacheBase using
    OpenGL Textures.
*/
template<typename MakeResourceClass> class IMOGLTextureCache : public TextureCacheBase {
  ResourceCache2<IMOGLTexture,MakeResourceClass> textures;
public:
  explicit IMOGLTextureCache(MakeResourceClass MakeTexture) :
    textures(MakeTexture)
  {
  }
  ~IMOGLTextureCache() {
  }
  void Clear() {
    textures.Clear();
  }
  void Update(const std::string& id) {
    textures[id];
  }
  void Bind(const std::string& id) {
    textures[id].Bind();
  }
  void UnBind() const {
    IMOGLTexture::UnBind();
  }
  const IMOGLTexture& Get(const std::string& id) {
    return textures[id];
  }
};

}

#endif // IMGUI_IMOGLTEXTURECACHE_HPP
