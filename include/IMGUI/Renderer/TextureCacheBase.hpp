#ifndef IMGUI_TEXTURECACHEBASE_HPP
#define IMGUI_TEXTURECACHEBASE_HPP

#include <string>

namespace IMGUI {

/** TextureCacheBase provides an interface for abstracting away texture
    caching and binding.
*/
class TextureCacheBase {
public:
  virtual ~TextureCacheBase() {
  }
  virtual void Clear() = 0;
  virtual void Update(const std::string& id) = 0;
  virtual void Bind(const std::string& id) = 0;
  virtual void UnBind() const = 0;
};

}

#endif // IMGUI_TEXTURECACHEBASE_HPP
