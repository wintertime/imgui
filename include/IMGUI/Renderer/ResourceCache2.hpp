#ifndef IMGUI_RESOURCECACHE2_HPP
#define IMGUI_RESOURCECACHE2_HPP

#include <string>
#include <map>
#include <stdexcept>

namespace IMGUI {

/** ResourceCache2 is a helper template for caching resources and calling
    into a function object for creating a missing resource when needed.
*/
template<typename R,typename MakeResourceClass> class ResourceCache2 {
  MakeResourceClass MakeResource;
  std::map<std::string,R> cache;
public:
  ResourceCache2(MakeResourceClass MakeResource_) :
    MakeResource(MakeResource_)
  {
  }
  ResourceCache2(const ResourceCache2&) = delete;
  const ResourceCache2& operator=(const ResourceCache2&) = delete;
  ~ResourceCache2() {}
  const R& operator[] (const std::string& id) {
    auto i(cache.find(id));
    if(i!=cache.cend())
      return i->second;
    auto p(cache.insert(make_pair(id,MakeResource(id))));
    if(!p.second)
      throw std::runtime_error("error inserting into ResourceCache");
    return p.first->second;
  }
  bool IsCached(const std::string& id) {
    return cache.find(id)!=cache.cend();
  }
  bool Forget(const std::string& id) {
    return cache.erase(id);
  }
  void Clear() {
    cache.clear();
  }
};

}

#endif // IMGUI_RESOURCECACHE2_HPP
