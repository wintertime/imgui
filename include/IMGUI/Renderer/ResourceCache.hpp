#ifndef IMGUI_RESOURCECACHE_HPP
#define IMGUI_RESOURCECACHE_HPP

#include <string>
#include <map>
#include <stdexcept>

namespace IMGUI {

/** ResourceCache is a helper template for caching resources and calling
    a function for creating a missing resource when needed.
*/
template<typename R,R (*MakeResource)(const std::string& id)> class ResourceCache {
  std::map<std::string,R> cache;
public:
  ResourceCache() {}
  ResourceCache(const ResourceCache&) = delete;
  const ResourceCache& operator=(const ResourceCache&) = delete;
  ~ResourceCache() {}
  const R& operator[] (const std::string& id) {
    auto i(cache.find(id));
    if(i!=cache.cend())
      return i->second;
    auto p(cache.insert(make_pair(id,MakeResource(id))));
    if(!p.second)
      throw std::runtime_error("error inserting into ResourceCache");
    return p.first->second;
  }
  bool IsCached(const std::string& id) {
    return cache.find(id)!=cache.cend();
  }
  bool Forget(const std::string& id) {
    return cache.erase(id);
  }
  void Clear() {
    cache.clear();
  }
};

}

#endif // IMGUI_RESOURCECACHE_HPP
