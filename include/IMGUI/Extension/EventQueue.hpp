#ifndef IMGUI_EVENTQUEUE_HPP
#define IMGUI_EVENTQUEUE_HPP

#include "IMGUI/Core/Event.hpp"
#include <deque>

namespace IMGUI {

/** EventQueue helps with storing Events and processing them one by one
*/
class EventQueue {
  std::deque<Event> events;
public:
  EventQueue();
  virtual ~EventQueue();
  virtual void PostEvent(const Event& event);
  virtual bool PollEvent(Event& event);
  bool HasEvent() const;
};

}

#endif // IMGUI_EVENTQUEUE_HPP
