#ifndef IMGUI_UTF8_STD_STRING_TO_SF_STRING_HPP
#define IMGUI_UTF8_STD_STRING_TO_SF_STRING_HPP

#include "SFML/System/String.hpp"
#include "SFML/System/Utf.hpp"

namespace IMGUI {

inline void UTF8_std_string_to_sf_String(const std::string& text,sf::String& sftext) {
  std::string::const_iterator i(text.cbegin());
  std::string::const_iterator e(text.cend());
  while(i!=e) {
    sf::Uint32 out;
    i=sf::Utf8::decode(i,e,out);
    sftext+=out;
  }
}

}

#endif // IMGUI_UTF8_STD_STRING_TO_SF_STRING_HPP
