#ifndef IMGUI_SFMLTEXTURECACHE_HPP
#define IMGUI_SFMLTEXTURECACHE_HPP

#include "IMGUI/Renderer/TextureCacheBase.hpp"
#include "IMGUI/Renderer/ResourceCache2.hpp"
#include "SFML/Graphics/Texture.hpp"
#include <memory>

namespace IMGUI {

/** SFMLTextureCache provides an implementation of TextureCacheBase using
    sf::Texture from SFML.
*/
template<typename MakeResourceClass> class SFMLTextureCache : public TextureCacheBase {
  ResourceCache2<std::shared_ptr<sf::Texture>,MakeResourceClass> textures;
public:
  explicit SFMLTextureCache(MakeResourceClass MakeTexture) :
    textures(MakeTexture)
  {
  }
  ~SFMLTextureCache() {
  }
  void Clear() {
    textures.Clear();
  }
  void Update(const std::string& id) {
    textures[id];
  }
  void Bind(const std::string& id) {
    sf::Texture::bind(textures[id].get());
  }
  void UnBind() const {
    sf::Texture::bind(nullptr);
  }
  const sf::Texture& Get(const std::string& id) {
    return *textures[id];
  }
};

}

#endif // IMGUI_SFMLTEXTURECACHE_HPP
