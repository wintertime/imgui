#ifndef IMGUI_SFMLRENDERER_HPP
#define IMGUI_SFMLRENDERER_HPP

#include "SFMLTextureCache.hpp"
#include "IMGUI/Core/SplitRenderer.hpp"

namespace sf {

class Font;
class RenderTarget;

}

namespace IMGUI {

/** SFMLRenderer got the best text output quality, but draws directly without
    caching Draw calls which complicates the main loop.
    If drawing to a RenderTexture it would be a good choice.
*/
class SFMLRenderer : virtual public SplitRenderer {
  sf::RenderTarget& renderTarget;
  const sf::Font& font;
  SFMLTextureCache<std::shared_ptr<sf::Texture> (*)(const std::string& id)>& textures;

public:
  SFMLRenderer(sf::RenderTarget& renderTarget_,const sf::Font& font_,SFMLTextureCache<std::shared_ptr<sf::Texture> (*)(const std::string& id)>& textures_);
  ~SFMLRenderer();

  void Render(const DrawItem& drawItem);

private:
  SFMLRenderer(const SFMLRenderer& other);
  const SFMLRenderer& operator=(const SFMLRenderer& other);
};

}

#endif // IMGUI_SFMLRENDERER_HPP
