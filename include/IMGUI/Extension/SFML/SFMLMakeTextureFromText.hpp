#ifndef IMGUI_SFMLMAKETEXTUREFROMTEXT_HPP
#define IMGUI_SFMLMAKETEXTUREFROMTEXT_HPP

#include <memory>

namespace sf {

class Font;
class Texture;

}

namespace IMGUI {

class SFMLMakeTextureFromText {
  const sf::Font& font;
public:
  SFMLMakeTextureFromText(const sf::Font& font_);
  std::shared_ptr<sf::Texture> operator() (const std::string& text);
};

}

#endif // IMGUI_SFMLMAKETEXTUREFROMTEXT_HPP
