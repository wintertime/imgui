#ifndef IMGUI_CACHEDSFMLRENDERER_HPP
#define IMGUI_CACHEDSFMLRENDERER_HPP

#include "SFMLRenderer.hpp"
#include "IMGUI/Renderer/CachedRenderer.hpp"

namespace IMGUI {

/** CachedSFMLRenderer is the currently prefered Renderer.
    It combines best text output quality and ease of use.
*/
class CachedSFMLRenderer : public SFMLRenderer, public CachedRenderer {
public:
  CachedSFMLRenderer(sf::RenderTarget& renderTarget_,const sf::Font& font_,SFMLTextureCache<std::shared_ptr<sf::Texture> (*)(const std::string& id)>& textures_);
  ~CachedSFMLRenderer();
};

}

#endif // IMGUI_CACHEDSFMLRENDERER_HPP
