#ifndef IMGUI_SFMLLOADTEXTURE_HPP
#define IMGUI_SFMLLOADTEXTURE_HPP

#include <memory>

namespace sf {

class Texture;

}

namespace IMGUI {

std::shared_ptr<sf::Texture> SFMLLoadTexture(const std::string& textureFileName);

}

#endif // IMGUI_SFMLLOADTEXTURE_HPP
