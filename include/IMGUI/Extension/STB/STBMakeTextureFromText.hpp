#ifndef IMGUI_STBMAKETEXTUREFROMTEXT_HPP
#define IMGUI_STBMAKETEXTUREFROMTEXT_HPP

#include "IMGUI/Renderer/IMOGLTexture.hpp"
#include <string>
#include <memory>

typedef struct stbtt_fontinfo stbtt_fontinfo;

namespace IMGUI {

class STBMakeTextureFromText {
  const char* font;
  std::shared_ptr<stbtt_fontinfo> fontinfo;

public:
  STBMakeTextureFromText(const char* font_);
  IMOGLTexture operator() (const std::string& text);
};

}

#endif // IMGUI_STBMAKETEXTUREFROMTEXT_HPP
