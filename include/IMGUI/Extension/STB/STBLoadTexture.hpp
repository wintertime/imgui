#ifndef IMGUI_STBLOADTEXTURE_HPP
#define IMGUI_STBLOADTEXTURE_HPP

#include "IMGUI/Renderer/IMOGLTexture.hpp"
#include <string>

namespace IMGUI {

IMOGLTexture STBLoadTexture(const std::string& textureFileName);

}

#endif // IMGUI_STBLOADTEXTURE_HPP
