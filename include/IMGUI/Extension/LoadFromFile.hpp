#ifndef IMGUI_LOADFROMFILE_HPP
#define IMGUI_LOADFROMFILE_HPP

#include <memory>

namespace IMGUI {

std::unique_ptr<char[]> LoadFromFile(const char* fileName,size_t& dataSize);

}

#endif // IMGUI_LOADFROMFILE_HPP
