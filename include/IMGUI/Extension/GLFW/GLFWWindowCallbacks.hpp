#ifndef IMGUI_GLFWWINDOWCALLBACKS_HPP
#define IMGUI_GLFWWINDOWCALLBACKS_HPP

extern "C" {
typedef struct GLFWwindow GLFWwindow;
}

namespace IMGUI {

class EventQueue;

/** SetGLFWWindowCallbacks sets the UserPointer and all window callbacks
    for the given GLFWwindow (error and monitor callbacks are not set).
    These callbacks translate the information on each call into an
    IMGUI::Event and call PostEvent to add it to the given EventQueue.
*/
void SetGLFWWindowCallbacks(GLFWwindow* window,EventQueue& eventQueue);
/** ResetGLFWWindowCallbacks resets the UserPointer and all window callbacks
    for the given GLFWwindow to remove them (error and monitor callbacks are
    not reset).
*/
void ResetGLFWWindowCallbacks(GLFWwindow* window);

}

#endif // IMGUI_GLFWWINDOWCALLBACKS_HPP
