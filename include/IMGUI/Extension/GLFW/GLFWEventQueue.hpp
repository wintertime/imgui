#ifndef IMGUI_GLFWEVENTQUEUE_HPP
#define IMGUI_GLFWEVENTQUEUE_HPP

#include "IMGUI/Extension/EventQueue.hpp"

extern "C" {
typedef struct GLFWwindow GLFWwindow;
}

namespace IMGUI {

/** GLFWEventQueue extends EventQueue to set up window callbacks and
    automatically call GLFWPollEvents and GLFWWaitEvents.
*/
class GLFWEventQueue : public EventQueue {
  GLFWwindow* window;
public:
  explicit GLFWEventQueue(GLFWwindow* window_);
  ~GLFWEventQueue();
  bool PollEvent(Event& event);
  void WaitEvent();
};

}

#endif // IMGUI_GLFWEVENTQUEUE_HPP
