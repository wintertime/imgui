#ifndef IMGUI_UTF8_STD_STRING_DECODE_HPP
#define IMGUI_UTF8_STD_STRING_DECODE_HPP

#include <string>

namespace IMGUI {

std::u32string UTF8_std_string_decode(const std::string& text);

}

#endif // IMGUI_UTF8_STD_STRING_DECODE_HPP
