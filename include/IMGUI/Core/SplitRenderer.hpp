#ifndef IMGUI_SPLITRENDERER_HPP
#define IMGUI_SPLITRENDERER_HPP

#include "RendererBase.hpp"

namespace IMGUI {

/** SplitRenderer is just an intermediate class to be derived from if convenient.
*/
class SplitRenderer : public RendererBase {
public:
  ~SplitRenderer();

  void Draw(DrawItem&& drawItem);

  virtual void Update(const DrawItem& drawItem);
  virtual void BeginRender();
  virtual void Render(const DrawItem& drawItem);
  virtual void EndRender();
};

}

#endif // IMGUI_SPLITRENDERER_HPP
