#ifndef IMGUI_WIDGETLOGIC_HPP
#define IMGUI_WIDGETLOGIC_HPP

#include "GUIFwd.hpp"

namespace IMGUI {

/* helpers for implementing more widgets */
/// InvisibleBox takes space up for layout, it is called first by most widgets to adjust their size.
void InvisibleBox(IRect& space,BorderConnect bc,glm::ivec2& size);
/// ButtonLogic is called by most button-like widgets using their own id after having calculated their layout already.
/// It calculates hot/active status and return value signals full click cycles done with a number of (1<<button).
int32_t ButtonLogic(int32_t id,Frame& frame,const IRect& space);

}

#endif // IMGUI_WIDGETLOGIC_HPP
