#ifndef IMGUI_DRAWBOX_HPP
#define IMGUI_DRAWBOX_HPP

#include "GUIFwd.hpp"
#include <string>

namespace IMGUI {

/* These functions translate GUI information into Draw calls. */
void DrawBackgroundBox(Frame& frame,const Style& style,const IRect& space);
void DrawForegroundBox(Frame& frame,const Style& style,const IRect& space,bool hot);
void DrawButtonBox(Frame& frame,const Style& style,const IRect& space,bool hot,bool active);
void DrawTextureInBox(Frame& frame,const Style& style,const IRect& space,const std::string& textureID,bool hot,bool active);
void DrawTextInBox(Frame& frame,const Style& style,const IRect& space,const std::string& text,bool hot,bool active);

}

#endif // IMGUI_DRAWBOX_HPP
