#ifndef IMGUI_UTF8_STD_STRING_DELETE_LAST_HPP
#define IMGUI_UTF8_STD_STRING_DELETE_LAST_HPP

#include <string>

namespace IMGUI {

void UTF8_std_string_delete_last(std::string& text);

}

#endif // IMGUI_UTF8_STD_STRING_DELETE_LAST_HPP
