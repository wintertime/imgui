#ifndef IMGUI_FRAME_HPP
#define IMGUI_FRAME_HPP

#include "GUI.hpp"

namespace IMGUI {

class DrawItem;
class RendererBase;
class Event;

/** Frame holds references to GUI and last event to reduce number of parameters
    to GUI functions and does some bookkeeping for each frame.
    It needs to be created once at start of a frame and kept alive till end of frame.
*/
class Frame {
public:
  GUI& gui;
private:
  RendererBase& renderer;
public:
  Event& event;
  /// A bitfield showing current status of mouse-buttons.
  int32_t mouseButtons;
  /// The current adjusted mouse position.
  /// Widgets should use mousePos instead of a mouse position from event.
  glm::ivec2 mousePos;

  Frame(GUI& gui_,RendererBase& renderer_,Event& event_);
  ~Frame();

  void Draw(DrawItem&& drawItem);
};

}

#endif // IMGUI_FRAME_HPP
