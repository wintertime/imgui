#ifndef IMGUI_GENID_HPP
#define IMGUI_GENID_HPP

#include <cstdint>

namespace IMGUI {

/** GenID is a helper for combined widgets to generate a fresh, unchanging, unique id
    for their child widgets.
*/
inline int32_t GenID(int32_t idParent,int32_t maxChilds,int32_t numberChild) {
  if(idParent==0 || idParent==-1) {
    int32_t temp((idParent+1+1)*maxChilds+(numberChild+1));
    if(temp!=0 && temp!=-1)
      return temp; // normal return for no parent
    // try to make best use of wrong input
    if(maxChilds==0 || maxChilds==-1) {
      temp=0x00010000^(((idParent&1)<<15)|((maxChilds&1)<<14))^(numberChild+1);
      if(temp!=0 && temp!=-1)
        return temp;
      return 0x00020000^temp;
    }
    temp=0x00020000^(((idParent&1)<<15)^((maxChilds+1)<<8)^(numberChild+1));
    if(temp!=0 && temp!=-1)
      return temp;
    return 0x00040000^temp;
  }
  if(maxChilds==0 || maxChilds==-1) {
    // try to recover from wrong number
    int32_t temp(0x00040000^((idParent+1+1+1)*(maxChilds+2+(numberChild+1))));
    if(temp!=0 && temp!=-1)
      return temp;
    return 0x00010000^temp;
  }
  int32_t temp((idParent+1+1)*maxChilds+(numberChild+1)); // cross fingers for globally unique number
  if(temp!=0 && temp!=-1)
    return temp; // normal case
  return 0x00070000^temp;
}

}

#endif // IMGUI_GENID_HPP
