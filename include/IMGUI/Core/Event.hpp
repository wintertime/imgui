#ifndef IMGUI_EVENT_HPP
#define IMGUI_EVENT_HPP

#include "glm/glm.hpp"
#include <cstdint>

namespace sf {

class Event;

}

namespace IMGUI {

/** Event keeps the needed input data in a convenient format for the GUI.
*/
class Event {
public:
  enum Type {
    None = 0,
    Paint = 0,
    MouseMove = 1,
    MouseWheel = 2+1,
    MouseButtonDown = 4+1,
    MouseButtonUp = 8+1,
    KeyDown = 0x400,
    KeyUp = 0x800,
    Text = 0x200,
    ResizeW = 0x1000,
    ResizeF = 0x2000,
    Reset = 0x4000,
    Close = 0x8000
  };
  enum Modifier {
    System  = 0x10000000,
    Alt     = 0x20000000,
    Ctrl    = 0x40000000,
    Shift   = 0x80000000,
    MaskMod = 0xf0000000,
    MaskKey = 0x0fffffff
  };

  Type type;
  int32_t data;
  glm::ivec2 point;
  sf::Event* sfEvent;

  Event() :
    type(Paint),
    data(0),
    point(),
    sfEvent(nullptr)
  {
  }

  Event(Type type_,int32_t data_,const glm::ivec2& point_) :
    type(type_),
    data(data_),
    point(point_),
    sfEvent(nullptr)
  {
  }

  Event(sf::Event& sfEvent);
};

}

#endif // IMGUI_EVENT_HPP
