#ifndef IMGUI_IRECT_HPP
#define IMGUI_IRECT_HPP

#include "glm/glm.hpp"

namespace IMGUI {

/** IRect is a helper structure for keeping available space for Widgets.
*/
class IRect {
public:
  glm::ivec2 pos;
  glm::ivec2 size;
  bool contains(const glm::ivec2& point) const;
};

inline bool IRect::contains(const glm::ivec2& point) const {
  return glm::all(glm::greaterThanEqual(point,pos)) && glm::all(glm::lessThan(point,pos+size));
}

}

#endif // IMGUI_IRECT_HPP
