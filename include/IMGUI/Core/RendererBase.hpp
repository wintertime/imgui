#ifndef IMGUI_RENDERERBASE_HPP
#define IMGUI_RENDERERBASE_HPP

namespace IMGUI {

class DrawItem;

/** RendererBase is a base class for implementing a backend for GUI drawing.
    Connecting it to your code by overwriting Draw is all you need to do.
*/
class RendererBase {
public:
  virtual ~RendererBase();

  virtual void Draw(DrawItem&& drawItem);
};

typedef RendererBase NullRenderer;

}

#endif // IMGUI_RENDERERBASE_HPP
