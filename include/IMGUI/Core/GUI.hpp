#ifndef IMGUI_GUI_HPP
#define IMGUI_GUI_HPP

#include "GUIFwd.hpp"

namespace IMGUI {

/** GUI holds the only global bookkeeping data for the IMGUI.
    It needs to be created once at gamestart and kept alive till the end.
*/
class GUI {
public:
  /// id of widget mouse is hovering at
  int32_t hot;
  /// id of widget hit with mousebutton down, while button is still not up
  int32_t active;
  /// bitfield with status of all mouse-buttons from last frame
  int32_t mouseButtons;
  /// mouse-position from last frame
  glm::ivec2 mousePos;
  /// false==block widgets from getting active while other button down already,
  /// true==allow for click with other button to activate a widget while holding button(s) which did not hit
  bool multiButton;

  GUI() :
    hot(0),
    active(0),
    mouseButtons(0),
    mousePos(-1,-1),
    multiButton(true)
  {
  }

};

}

#endif // IMGUI_GUI_HPP
