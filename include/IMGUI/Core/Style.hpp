#ifndef IMGUI_STYLE_HPP
#define IMGUI_STYLE_HPP

#include "glm/glm.hpp"
#include "glm/gtc/type_precision.hpp"

namespace IMGUI {

/** Style keeps some data for configuring draw style of the GUI.
    You or any glue widget could make a new Style at any time
    to support more than one Style at once.
*/
class Style {
public:
  glm::u8vec4 background;
  glm::u8vec4 foreground;
  glm::u8vec4 border;
  glm::u8vec4 texture;
  glm::u8vec4 text;
  glm::u8vec4 hot;
  glm::u8vec4 active;

  Style() :
    background(0,0,0,255),
    foreground(127,127,127,255),
    border(63,63,63,255),
    texture(255,255,255,255),
    text(255,255,255,255),
    hot(0,0x80,0,0x80),
    active(0x80,0,0,0x80)
  {
  }

  Style(const glm::u8vec4& background_,const glm::u8vec4& foreground_,const glm::u8vec4& border_,const glm::u8vec4& texture_,const glm::u8vec4& text_,const glm::u8vec4& hot_,const glm::u8vec4& active_) :
    background(background_),
    foreground(foreground_),
    border(border_),
    texture(texture_),
    text(text_),
    hot(hot_),
    active(active_)
  {
  }

  static glm::u8vec4 MixColors(const glm::u8vec4& color,const glm::u8vec4& change) {
    return glm::u8vec4((glm::u16vec4(color)*glm::u16(255-change.a))/glm::u16(255)) + change;
  }
};

}

#endif // IMGUI_STYLE_HPP
