#ifndef IMGUI_UTF8_STD_STRING_ADD_CONVERTED_UTF32_CHAR_HPP
#define IMGUI_UTF8_STD_STRING_ADD_CONVERTED_UTF32_CHAR_HPP

#include <string>

namespace IMGUI {

void UTF8_std_string_add_converted_UTF32_char(std::string& text,char32_t character);

}

#endif // IMGUI_UTF8_STD_STRING_ADD_CONVERTED_UTF32_CHAR_HPP
