#ifndef IMGUI_BORDERCONNECT_HPP
#define IMGUI_BORDERCONNECT_HPP

namespace IMGUI {

/** BorderConnect is for choosing the preferred position inside the parent widget and
    also for maximising in x and/or y direction when connected to opposite borders.
*/
enum BorderConnect {
  none=0,
  up=1<<0,
  down=1<<1,
  left=1<<2,
  right=1<<3,
  center=none,
  upleft=up|left,
  upright=up|right,
  downleft=down|left,
  downright=down|right,
  updown=up|down,
  leftright=left|right,
  updownleftright=up|down|left|right,
  maximized=updownleftright,
  // todo: implement handling of minimized and invisible
  // currently both are treated like none
  minimized=1<<4,
  invisible=1<<5
};

}

#endif // IMGUI_BORDERCONNECT_HPP
