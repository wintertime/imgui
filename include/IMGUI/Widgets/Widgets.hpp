#ifndef IMGUI_WIDGETS_HPP
#define IMGUI_WIDGETS_HPP

#include "BoxWidgets.hpp"
#include "ButtonWidgets.hpp"
#include "MenuWidgets.hpp"
#include "BarWidgets.hpp"
#include "EditWidgets.hpp"

/*
information about all widgets
*****************************

inputs:
-------
id:
- globally unique id number for the called widget instance,
- one per type is not enough,
- be careful to not use the same number if adding more than 1 widgets in a loop
- needs to be the same id for the same widget in different frames
frame:
- needs to be created anew every time before a new event is given to the widget hierarchy
- needs to be destroyed after all calls to widgets using that event are done
- there is some magic happening in constructor/destructor to make the gui work
style:
- contains colors to be used
- sometimes its useful for widgets to make a copy and modify it before calling contained widgets with it
- can be used to hide certain widget features using transparency
space:
- hard limit on available space for the widget and subwidgets to use
size:
- suggested optimum size of the widget
- can be shrunk or enlarged as needed
bc:
- which borders of the space the widget should be placed at
- implicitly tells if it should be enlarged to touch opposite borders of space
text:
- text to be shown on widgets
others:
- current status of widgets that have one

outputs:
--------
frame:
- modified rarely, used to save widget status (hot/active) indirectly in gui
space:
- the actual position and size the widget is taking up
size:
- the actual size the widget is taking up
text/others:
- the possibly changed status of widgets
return value:
- is normally 0 or false if no interesting event happened
- the status of the widget or registered button click cycles change this
*/

namespace IMGUI {

/* ideas for widgets to implement later */
// subbox to nicely align child widgets?
// int32_t CombinerBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,...);
// Background and possibly much Text and maybe scrollbar to move it
// bool TextViewerBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,const std::string& text,glm::ivec2& size);
// possibly movable TextButton plus small Buttons for min,optimum,max size,closing
// int32_t TitleBar(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect& bc,const std::string& text);
// TitleBar plus Box inside, maybe resizable/movable
// would return the available client area in spaceForBox
// bool Window(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect& bc,const std::string& text,IRect& spaceForBox);

}

#endif // IMGUI_WIDGETS_HPP
