#ifndef IMGUI_BOXWIDGETS_HPP
#define IMGUI_BOXWIDGETS_HPP

#include "IMGUI/Core/GUIFwd.hpp"
#include <string>

namespace IMGUI {

/* basic widgets */
/// BackgroundBox just draws a box in background color, with no interaction.
void BackgroundBox(Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size);
/// ForegroundBox just draws a box in foreground color, it currently highlights when hot (hovering mouse) but that may change.
void ForegroundBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size);
/// TextBox calls BackgroundBox and draws static text on it.
void TextBox(Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& text);
/// ForegroundTextBox is a ForegroundBox with highlighted static text on it.
void ForegroundTextBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& text);
/// TextureBox draws a static texture.
void TextureBox(Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& texture);
/// ForegroundTextureBox is similar to a TextureBox, but with highlighting.
void ForegroundTextureBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& texture);

}

#endif // IMGUI_BOXWIDGETS_HPP
