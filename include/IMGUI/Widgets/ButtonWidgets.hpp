#ifndef IMGUI_BUTTONWIDGETS_HPP
#define IMGUI_BUTTONWIDGETS_HPP

#include "IMGUI/Core/GUIFwd.hpp"
#include <string>
#include <vector>

namespace IMGUI {

/* clickable widgets */
/// Button is in foreground color with highlights depending on status.
int32_t Button(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size);
/// CheckBox is like a button with a durable status, it shows X when checked.
/// It always returns same value as the checked argument, that possibly got modified by a click cycle.
bool CheckBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,bool& checked);
/// TextButton is a button with static, highlighted text on it.
int32_t TextButton(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& text);
/// TextButton2 is a button with text on it that switches while activated.
int32_t TextButton2(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& text,const std::string& text2);
/// TextCheckBox works like a CheckBox, but shows different text depending on status.
bool TextCheckBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& text,const std::string& text2,bool& checked);
/// TextureButton is a button showing a texture.
int32_t TextureButton(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& texture);
/// TextureButton2 is a button showing a texture that switches while activated.
int32_t TextureButton2(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& texture,const std::string& texture2);
/// TextureCheckBox works like a CheckBox, but shows different textures depending on status.
bool TextureCheckBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& texture,const std::string& texture2,bool& checked);
/// TextButtonSet shows many TextButtons in a set of consistent size.
/// @returns number of clicked button beginning with 1 for first, as 0 is already used for no event
int32_t TextButtonSet(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::vector<std::string>& itemtext);
/// TextRadioButtonSet is like TextButtonSet, but most buttons are shown in background color.
/// Only a single button can be activated and is shown in foreground color.
/// If called with a number in selected for a button thats not available in the array (like 0),
/// no change is made and no button is selected.
/// @returns currently selected button
int32_t TextRadioButtonSet(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::vector<std::string>& itemtext,int32_t& selected);
/// TextureButtonSet is like TextButtonSet, but with textures instead.
int32_t TextureButtonSet(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::vector<std::string>& itemtextures);
/// TextureRadioButtonSet is like TextRadioButtonSet, but most with textures instead.
int32_t TextureRadioButtonSet(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::vector<std::string>& itemtextures,int32_t& selected);

}

#endif // IMGUI_BUTTONWIDGETS_HPP
