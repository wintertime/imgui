#ifndef IMGUI_EDITWIDGETS_HPP
#define IMGUI_EDITWIDGETS_HPP

#include "IMGUI/Core/GUIFwd.hpp"
#include <string>

namespace IMGUI {

/// EditBox provides simple text input at end of a string.
/// @returns (1+button) on click, -1 on backspace, -2 on character input
int32_t EditBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,std::string& text);

}

#endif // IMGUI_EDITWIDGETS_HPP
