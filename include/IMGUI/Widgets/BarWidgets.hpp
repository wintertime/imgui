#ifndef IMGUI_BARWIDGETS_HPP
#define IMGUI_BARWIDGETS_HPP

#include "IMGUI/Core/GUIFwd.hpp"

namespace IMGUI {

/* more advanced, movable bar widgets */
// todo: These widgets need testing.
/// TrackBar is, surprise, a track bar with a variable size knob.
/// It allows adjusting values between low and high.
bool TrackBar(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,int& breadth,int low,int high,int& poslow,int& poshigh);
/// ScrollBar provides 2 arrow Buttons and a track bar inbetween to be adjusted by them.
bool ScrollBar(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,int& breadth,int low,int high,int& poslow,int& poshigh);

}

#endif // IMGUI_BARWIDGETS_HPP
