#ifndef IMGUI_MENUWIDGETS_HPP
#define IMGUI_MENUWIDGETS_HPP

#include "IMGUI/Core/GUIFwd.hpp"
#include <string>
#include <vector>

namespace IMGUI {

/* more advanced widgets for menus */
// todo: These widgets need testing.
/// Menu is like a TextButton and when clicked adds a TextButtonSet for the menu items.
int32_t Menu(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,const std::string& text,const std::vector<std::string>& itemtext,int& textheight,bool& opened);
/// TextureMenu is like a Menu with textures instead of text for the menu items.
int32_t TextureMenu(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,const std::string& texture,const std::vector<std::string>& itemtextures,int& textureheight,bool& opened);

}

#endif // IMGUI_MENUWIDGETS_HPP
