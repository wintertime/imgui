#include "IMGUI/Core/UTF8_std_string_add_converted_UTF32_char.hpp"

namespace IMGUI {

void UTF8_std_string_add_converted_UTF32_char(std::string& text,char32_t character) {
  if(character<=0x7F)
    text.push_back(character);
  else if(character<=0x7FF) {
    text.push_back((character>>6)|0xC0);
    text.push_back((character&0x3F)|0x80);
  } else if(character<=0xFFFF) {
    text.push_back((character>>12)|0xE0);
    text.push_back((character>>6)|0x80);
    text.push_back((character&0x3F)|0x80);
  } else if(character<=0x10FFFF) {
    text.push_back((character>>18)|0xF0);
    text.push_back((character>>12)|0x80);
    text.push_back((character>>6)|0x80);
    text.push_back((character&0x3F)|0x80);
  } else {
    // use replacement character 0xFFFD for illegal codes
    text.push_back(0xEF);
    text.push_back(0xBF);
    text.push_back(0xBD);
  }
}

}
