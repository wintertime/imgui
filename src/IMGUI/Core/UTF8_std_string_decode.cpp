#include "IMGUI/Core/UTF8_std_string_decode.hpp"

namespace IMGUI {

std::u32string UTF8_std_string_decode(const std::string& text) {
  std::u32string codepoints;
  size_t bytes=text.size();
  if(bytes!=0) {
    codepoints.reserve(bytes);
    for(size_t i=0;i<bytes;) {
      unsigned char uc0=text[i];
      char32_t cp;
      if(uc0<=0x7FU) {
        cp=uc0;
        ++i;
      } else if((uc0&0xE0U)==0xC0U) {
        if(i+2<=bytes) {
          unsigned char uc1=text[i+1];
          if((uc1&0xC0U)==0x80U) {
            cp=((uc0&0x1FU)<<6)|(uc1&0x3FU);
            if(cp<=0x7FU)
              cp=0x00FFFDU;
            i+=2;
          } else {
            cp=0x00FFFDU;
            ++i;
          }
        } else {
          cp=0x00FFFDU;
          ++i;
        }
      } else if((uc0&0xF0U)==0xE0U) {
        if(i+3<=bytes) {
          unsigned char uc1=text[i+1];
          if((uc1&0xC0U)==0x80U) {
            unsigned char uc2=text[i+2];
            if((uc2&0xC0U)==0x80U) {
              cp=((uc0&0x0FU)<<12)|((uc1&0x3FU)<<6)|(uc2&0x3FU);
              if(cp<=0x7FFU || (cp>=0xD800U && cp<=0xDFFFU))
                cp=0x00FFFDU;
              i+=3;
            } else {
              cp=0x00FFFDU;
              i+=2;
            }
          } else {
            cp=0x00FFFDU;
            ++i;
          }
        } else {
          cp=0x00FFFDU;
          i+=2;
        }
      } else if((uc0&0xF8U)==0xF0U) {
        if(i+4<=bytes) {
          unsigned char uc1=text[i+1];
          if((uc1&0xC0U)==0x80U) {
            unsigned char uc2=text[i+2];
            if((uc2&0xC0U)==0x80U) {
              unsigned char uc3=text[i+3];
              if((uc3&0xC0U)==0x80U) {
                cp=((uc0&0x07U)<<18)|((uc1&0x3FU)<<12)|((uc2&0x3FU)<<6)|(uc3&0x3FU);
                if(cp<=0xFFFFU || cp>0x10FFFFU)
                  cp=0x00FFFDU;
                i+=4;
              } else {
                cp=0x00FFFDU;
                i+=3;
              }
            } else {
              cp=0x00FFFDU;
              i+=2;
            }
          } else {
            cp=0x00FFFDU;
            ++i;
          }
        } else {
          cp=0x00FFFDU;
          i+=3;
        }
      } else {
        cp=0x00FFFDU;
        if((uc0&0xC0U)==0x80U || (uc0&0xFCU)==0xF8U) {
          if(i+5<=bytes) {
            unsigned char uc1=text[i+1];
            if((uc1&0xC0U)==0x80U) {
              uc1=text[i+2];
              if((uc1&0xC0U)==0x80U) {
                uc1=text[i+3];
                if((uc1&0xC0U)==0x80U) {
                  uc1=text[i+4];
                  if((uc1&0xC0U)==0x80U)
                    i+=5;
                  else
                    i+=4;
                } else
                  i+=3;
              } else
                i+=2;
            } else
              ++i;
          } else
            i+=4;
        } else {
          if(i+5<=bytes) {
            unsigned char uc1=text[i+1];
            if((uc1&0xC0U)==0x80U) {
              uc1=text[i+2];
              if((uc1&0xC0U)==0x80U) {
                uc1=text[i+3];
                if((uc1&0xC0U)==0x80U) {
                  uc1=text[i+4];
                  if((uc1&0xC0U)==0x80U) {
                    uc1=text[i+5];
                    if((uc1&0xC0U)==0x80U)
                      i+=6;
                    else
                      i+=5;
                  } else
                    i+=4;
                } else
                  i+=3;
              } else
                i+=2;
            } else
              ++i;
          } else
            i+=5;
        }
      }
      codepoints.push_back(cp);
    }
  }
  return codepoints;
}

}
