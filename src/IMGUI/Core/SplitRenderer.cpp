#include "IMGUI/Core/SplitRenderer.hpp"

namespace IMGUI {

SplitRenderer::~SplitRenderer() {
}

void SplitRenderer::Draw(DrawItem&& drawItem) {
  BeginRender();
  Render(drawItem);
  EndRender();
}

void SplitRenderer::Update(const DrawItem& /*drawItem*/) {
}

void SplitRenderer::BeginRender() {
}

void SplitRenderer::Render(const DrawItem& /*drawItem*/) {
}

void SplitRenderer::EndRender() {
}

}
