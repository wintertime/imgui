#include "IMGUI/Core/WidgetLogic.hpp"
#include "IMGUI/Core/Frame.hpp"
#include "IMGUI/Core/Event.hpp"

namespace IMGUI {

void InvisibleBox(IRect& space,BorderConnect bc,glm::ivec2& size) {
  if(bc&left) {
    if(bc&right) {
      size.x=space.size.x;
    } else {
      if(space.size.x-size.x>0)
        space.size.x=size.x;
    }
  } else {
    if(bc&right) {
      if(space.size.x-size.x>0) {
        space.pos.x+=space.size.x-size.x;
        space.size.x=size.x;
      }
    } else {
      if(space.size.x-size.x>0) {
        space.pos.x+=(space.size.x-size.x)/2;
        space.size.x=size.x;
      }
    }
  }
  if(bc&up) {
    if(bc&down) {
      size.y=space.size.y;
    } else {
      if(space.size.y-size.y>0)
        space.size.y=size.y;
    }
  } else {
    if(bc&down) {
      if(space.size.y-size.y>0) {
        space.pos.y+=space.size.y-size.y;
        space.size.y=size.y;
      }
    } else {
      if(space.size.y-size.y>0) {
        space.pos.y+=(space.size.y-size.y)/2;
        space.size.y=size.y;
      }
    }
  }
}

int32_t ButtonLogic(int32_t id,Frame& frame,const IRect& space) {
  if(space.contains(frame.mousePos)) {
    frame.gui.hot=id;
    if(frame.gui.active==0 && frame.event.type==Event::MouseButtonDown)
      frame.gui.active=id;
  }
  if(frame.gui.hot==id && frame.gui.active==id && frame.event.type==Event::MouseButtonUp)
    return frame.event.data; // triggered
  return 0;
}

}
