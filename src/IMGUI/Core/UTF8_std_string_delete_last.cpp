#include "IMGUI/Core/UTF8_std_string_delete_last.hpp"

namespace IMGUI {

void UTF8_std_string_delete_last(std::string& text) {
  while(!text.empty() && (text.back()&0xC0)==0x80)
    text.pop_back();
  if(!text.empty())
    text.pop_back();
}

}
