#include "IMGUI/Core/DrawBox.hpp"
#include "IMGUI/Core/Frame.hpp"
#include "IMGUI/Core/Style.hpp"
#include "IMGUI/Core/DrawItem.hpp"

namespace IMGUI {

void DrawBackgroundBox(Frame& frame,const Style& style,const IRect& space) {
  if(style.background.a!=0)
    frame.Draw(DrawItem(space,style.background,DrawItem::solid));
}

void DrawForegroundBox(Frame& frame,const Style& style,const IRect& space,bool hot) {
  glm::u8vec4 color=style.foreground;
  if(hot)
    color=style.MixColors(color,style.hot);
  if(color.a!=0)
    frame.Draw(DrawItem(space,color,DrawItem::solid));
}

void DrawButtonBox(Frame& frame,const Style& style,const IRect& space,bool hot,bool active) {
  glm::u8vec4 color=style.foreground;
  if(hot)
    color=style.MixColors(color,style.hot);
  if(active)
    color=style.MixColors(color,style.active);
  if(color.a!=0)
    frame.Draw(DrawItem(space,color,DrawItem::solid));
}

void DrawTextureInBox(Frame& frame,const Style& style,const IRect& space,const std::string& textureID,bool hot,bool active) {
  glm::u8vec4 color=style.texture;
  if(hot)
    color=style.MixColors(color,style.hot);
  if(active)
    color=style.MixColors(color,style.active);
  if(color.a!=0)
    frame.Draw(DrawItem(space,color,DrawItem::texture,textureID));
}

void DrawTextInBox(Frame& frame,const Style& style,const IRect& space,const std::string& text,bool hot,bool active) {
  glm::u8vec4 color=style.text;
  if(hot)
    color=style.MixColors(color,style.hot);
  if(active)
    color=style.MixColors(color,style.active);
  if(color.a!=0)
    frame.Draw(DrawItem(space,color,DrawItem::text,text));
}

}
