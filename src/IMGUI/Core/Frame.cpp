#include "IMGUI/Core/Frame.hpp"
#include "IMGUI/Core/Event.hpp"
#include "IMGUI/Core/RendererBase.hpp"
#include <utility>

namespace IMGUI {

Frame::Frame(GUI& gui_,RendererBase& renderer_,Event& event_) :
  gui(gui_),
  renderer(renderer_),
  event(event_),
  mouseButtons(gui.mouseButtons),
  mousePos((event_.type&Event::MouseMove)?event_.point:gui.mousePos)
{
  gui.hot=0;
  switch(event.type) {
    case Event::MouseButtonDown :
      mouseButtons|=event.data;
      break;
    case Event::MouseButtonUp:
      mouseButtons&=~event.data;
      break;
    case Event::ResizeW:
    case Event::ResizeF:
    case Event::Reset:
      gui.active=0;
      gui.mouseButtons=0;
      mousePos.x=-1;
      mousePos.y=-1;
      break;
    default:
      ;
  }
}

Frame::~Frame() {
  if(mouseButtons==0 || event.type==Event::MouseButtonUp)
    gui.active=0;
  else if(!gui.multiButton && gui.active==0)
    gui.active=-1;
  gui.mouseButtons=mouseButtons;
  gui.mousePos=mousePos;
}

void Frame::Draw(DrawItem&& drawItem) {
  renderer.Draw(std::move(drawItem));
}

}
