#include "IMGUI/Core/DrawItem.hpp"

namespace IMGUI {

DrawItem::DrawItem() :
  rect(),
  color(0,0,0,0),
  type(nothing),
  str()
{
}

DrawItem::DrawItem(const IRect& rect_,const glm::u8vec4& color_,Type type_,const std::string& str_) :
  rect(rect_),
  color(color_),
  type(type_),
  str(str_)
{
}

DrawItem::DrawItem(DrawItem&& di) :
  rect(di.rect),
  color(di.color),
  type(di.type),
  str(std::move(di.str))
{
}

DrawItem::DrawItem(const DrawItem& di) :
  rect(di.rect),
  color(di.color),
  type(di.type),
  str(di.str)
{
}

const DrawItem& DrawItem::operator=(DrawItem&& di) {
  if(this!=&di) {
    rect=di.rect;
    color=di.color;
    type=di.type;
    str=std::move(di.str);
  }
  return *this;
}

const DrawItem& DrawItem::operator=(const DrawItem& di) {
  if(this!=&di) {
    rect=di.rect;
    color=di.color;
    type=di.type;
    str=di.str;
  }
  return *this;
}

DrawItem::~DrawItem() {
}

}
