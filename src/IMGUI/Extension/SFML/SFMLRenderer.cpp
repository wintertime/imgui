#include "IMGUI/Extension/SFML/SFMLRenderer.hpp"
#include "IMGUI/Extension/SFML/UTF8_std_string_to_sf_String.hpp"
#include "IMGUI/Core/DrawItem.hpp"
#include "SFML/Graphics/Vertex.hpp"
#include "SFML/Graphics/Text.hpp"
#include "SFML/Graphics/RenderTarget.hpp"

namespace IMGUI {

SFMLRenderer::SFMLRenderer(sf::RenderTarget& renderTarget_,const sf::Font& font_,SFMLTextureCache<std::shared_ptr<sf::Texture> (*)(const std::string& id)>& textures_) :
  renderTarget(renderTarget_),
  font(font_),
  textures(textures_)
{
}

SFMLRenderer::~SFMLRenderer() {
}

void SFMLRenderer::Render(const DrawItem& drawItem) {
  switch(drawItem.GetType()) {
  default:
    return;
  case DrawItem::solid:
    {
      const glm::u8vec4& vcolor(drawItem.GetColor());
      sf::Color color(vcolor.r,vcolor.g,vcolor.b,vcolor.a);
      const IRect& rect(drawItem.GetRect());
      sf::Vertex vertices[] = {
        sf::Vertex(sf::Vector2f(rect.pos.x            ,rect.pos.y            ),color),
        sf::Vertex(sf::Vector2f(rect.pos.x            ,rect.pos.y+rect.size.y),color),
        sf::Vertex(sf::Vector2f(rect.pos.x+rect.size.x,rect.pos.y+rect.size.y),color),
        sf::Vertex(sf::Vector2f(rect.pos.x+rect.size.x,rect.pos.y+rect.size.y),color),
        sf::Vertex(sf::Vector2f(rect.pos.x+rect.size.x,rect.pos.y            ),color),
        sf::Vertex(sf::Vector2f(rect.pos.x            ,rect.pos.y            ),color)
      };
      renderTarget.draw(vertices,6,sf::Triangles);
      return;
    }
  case DrawItem::texture:
    {
      const glm::u8vec4& vcolor(drawItem.GetColor());
      sf::Color color(vcolor.r,vcolor.g,vcolor.b,vcolor.a);
      const IRect& rect(drawItem.GetRect());
      const sf::Texture& texture(textures.Get(drawItem.GetStr()));
      auto textureSize(texture.getSize());
      sf::Vertex vertices[] = {
        sf::Vertex(sf::Vector2f(rect.pos.x            ,rect.pos.y            ),color,sf::Vector2f(0.f          ,0.f          )),
        sf::Vertex(sf::Vector2f(rect.pos.x            ,rect.pos.y+rect.size.y),color,sf::Vector2f(0.f          ,textureSize.y)),
        sf::Vertex(sf::Vector2f(rect.pos.x+rect.size.x,rect.pos.y+rect.size.y),color,sf::Vector2f(textureSize.x,textureSize.y)),
        sf::Vertex(sf::Vector2f(rect.pos.x+rect.size.x,rect.pos.y+rect.size.y),color,sf::Vector2f(textureSize.x,textureSize.y)),
        sf::Vertex(sf::Vector2f(rect.pos.x+rect.size.x,rect.pos.y            ),color,sf::Vector2f(textureSize.x,0.f          )),
        sf::Vertex(sf::Vector2f(rect.pos.x            ,rect.pos.y            ),color,sf::Vector2f(0.f          ,0.f          ))
      };
      renderTarget.draw(vertices,6,sf::Triangles,&texture);
      return;
    }
  case DrawItem::text:
    {
      sf::String sfstring;
      UTF8_std_string_to_sf_String(drawItem.GetStr(),sfstring);
      const IRect& rect(drawItem.GetRect());
      sf::Text sftext(sfstring,font,rect.size.y);
      sftext.setPosition(rect.pos.x,rect.pos.y);
      const glm::u8vec4& vcolor(drawItem.GetColor());
      sftext.setColor(sf::Color(vcolor.r,vcolor.g,vcolor.b,vcolor.a));
      sf::FloatRect bounds(sftext.getLocalBounds());
      sftext.setScale(rect.size.x/(bounds.left*2.f+bounds.width),rect.size.y/(bounds.top*2.f+bounds.height));
      renderTarget.draw(sftext);
    }
  }
}

}
