#include "IMGUI/Extension/SFML/SFMLLoadTexture.hpp"
#include "SFML/Graphics/Texture.hpp"
#include <stdexcept>

namespace IMGUI {

std::shared_ptr<sf::Texture> SFMLLoadTexture(const std::string& textureFileName) {
  std::shared_ptr<sf::Texture> texture(std::make_shared<sf::Texture>());
  if(!texture->loadFromFile(textureFileName))
    throw std::runtime_error("error loading texture "+textureFileName);
  texture->setSmooth(true);
  return std::move(texture);
}

}
