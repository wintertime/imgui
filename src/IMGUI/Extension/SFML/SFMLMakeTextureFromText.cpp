#include "IMGUI/Extension/SFML/SFMLMakeTextureFromText.hpp"
#include "IMGUI/Extension/SFML/UTF8_std_string_to_sf_String.hpp"
#include "SFML/System/Thread.hpp"
#include "SFML/Graphics/Text.hpp"
#include "SFML/Graphics/RenderTexture.hpp"
#include <stdexcept>

namespace IMGUI {

namespace {

struct TextTexture {
  const std::string* text;
  const sf::Font* font;
  std::shared_ptr<sf::Texture> texture;
};

void TextTextureThreadFunc(TextTexture* info) {
  sf::String sfstring;
  UTF8_std_string_to_sf_String(*info->text,sfstring);
  sf::Text sftext(sfstring,*info->font,64);
  sf::FloatRect bounds(sftext.getLocalBounds());
  sf::RenderTexture rtex;
  if(!rtex.create(bounds.left*2.f+bounds.width,bounds.top*2.f+bounds.height))
    return;
  rtex.clear(sf::Color::Transparent);
  rtex.draw(sftext);
  rtex.display();
  info->texture.reset(new sf::Texture(rtex.getTexture()));
  info->texture->setSmooth(true);
}

}

SFMLMakeTextureFromText::SFMLMakeTextureFromText(const sf::Font& font_) :
  font(font_)
{
}

std::shared_ptr<sf::Texture> SFMLMakeTextureFromText::operator() (const std::string& text) {
  TextTexture info = {&text,&font,nullptr};
  sf::Thread texturemaker(TextTextureThreadFunc,&info);
  texturemaker.launch();
  texturemaker.wait();
  if(!info.texture)
    throw std::runtime_error("error creating Texture for text");
  return info.texture;
}

}
