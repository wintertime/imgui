#include "IMGUI/Extension/SFML/CachedSFMLRenderer.hpp"

namespace IMGUI {

CachedSFMLRenderer::CachedSFMLRenderer(sf::RenderTarget& renderTarget_,const sf::Font& font_,SFMLTextureCache<std::shared_ptr<sf::Texture> (*)(const std::string& id)>& textures_) :
  SFMLRenderer(renderTarget_,font_,textures_),
  CachedRenderer()
{
}

CachedSFMLRenderer::~CachedSFMLRenderer() {
}

}
