#include "IMGUI/Core/Event.hpp"
#include "SFML/Window/Event.hpp"

namespace IMGUI {

Event::Event(sf::Event& sfEvent) :
  type(None),
  data(0),
  point(),
  sfEvent(&sfEvent)
{
  switch(sfEvent.type) {
    case sf::Event::MouseMoved:
      type=MouseMove;
      point.x=sfEvent.mouseMove.x;
      point.y=sfEvent.mouseMove.y;
      break;
    case sf::Event::MouseWheelMoved:
      type=MouseWheel;
      data=sfEvent.mouseWheel.delta;
      point.x=sfEvent.mouseWheel.x;
      point.y=sfEvent.mouseWheel.y;
      break;
    case sf::Event::MouseButtonPressed:
      type=MouseButtonDown;
      data=(1<<sfEvent.mouseButton.button);
      point.x=sfEvent.mouseButton.x;
      point.y=sfEvent.mouseButton.y;
      break;
    case sf::Event::MouseButtonReleased:
      type=MouseButtonUp;
      data=(1<<sfEvent.mouseButton.button);
      point.x=sfEvent.mouseButton.x;
      point.y=sfEvent.mouseButton.y;
      break;
    case sf::Event::KeyPressed:
      type=KeyDown;
      data=sfEvent.key.code&MaskKey;
      if(sfEvent.key.alt)
        data|=Alt;
      if(sfEvent.key.control)
        data|=Ctrl;
      if(sfEvent.key.shift)
        data|=Shift;
      if(sfEvent.key.system)
        data|=System;
      break;
    case sf::Event::KeyReleased:
      type=KeyUp;
      data=sfEvent.key.code&MaskKey;
      if(sfEvent.key.alt)
        data|=Alt;
      if(sfEvent.key.control)
        data|=Ctrl;
      if(sfEvent.key.shift)
        data|=Shift;
      if(sfEvent.key.system)
        data|=System;
      break;
    case sf::Event::TextEntered:
      type=Text;
      data=sfEvent.text.unicode;
    case sf::Event::Resized:
      type=ResizeW;
      point.x=sfEvent.size.width;
      point.y=sfEvent.size.height;
      break;
    case sf::Event::LostFocus:
    case sf::Event::GainedFocus:
    case sf::Event::MouseLeft:
    case sf::Event::MouseEntered:
      type=Reset;
      point.x=-1;
      point.y=-1;
      break;
    case sf::Event::Closed:
      type=Close;
      point.x=-1;
      point.y=-1;
      break;
    default:
      ;
  }
}

}
