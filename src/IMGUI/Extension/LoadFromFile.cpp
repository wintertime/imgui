#include "IMGUI/Extension/LoadFromFile.hpp"
#include <stdio.h>

namespace IMGUI {

std::unique_ptr<char[]> LoadFromFile(const char* fileName,size_t& dataSize) {
  FILE* lf=fopen(fileName,"rb");
  if(!lf)
    return {};
  setvbuf(lf,NULL,_IONBF,0);
  if(fseek(lf,0,SEEK_END)!=0)
    goto close_and_error;
  size_t size;
  {
    long fileSize=ftell(lf);
    if(fileSize==-1L)
      goto close_and_error;
    size=fileSize;
  }
  rewind(lf);
  {
    std::unique_ptr<char[]> ptr=std::unique_ptr<char[]>(new char[size]);
    if(!ptr)
      goto close_and_error;
    {
      size_t readSize=fread(ptr.get(),sizeof(char),size,lf);
      if(readSize!=size)
        goto close_and_error;
    }
    if(ferror(lf) || fgetc(lf)!=EOF)
      goto close_and_error;
    if(fclose(lf)!=0)
      return {};
    dataSize=size;
    return ptr;
  }
close_and_error:
  fclose(lf);
  return {};
}

}
