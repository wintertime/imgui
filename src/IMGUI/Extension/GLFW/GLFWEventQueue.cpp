#include "IMGUI/Extension/GLFW/GLFWEventQueue.hpp"
#include "IMGUI/Extension/GLFW/GLFWWindowCallbacks.hpp"
#include "GLFW/glfw3.h"

namespace IMGUI {

GLFWEventQueue::GLFWEventQueue(GLFWwindow* window_) :
  EventQueue(),
  window(window_)
{
  SetGLFWWindowCallbacks(window_,*this);
}

GLFWEventQueue::~GLFWEventQueue() {
  ResetGLFWWindowCallbacks(window);
}

bool GLFWEventQueue::PollEvent(Event& event) {
  if(EventQueue::PollEvent(event))
    return true;
  glfwPollEvents();
  return EventQueue::PollEvent(event);
}

void GLFWEventQueue::WaitEvent() {
  while(!HasEvent())
    glfwWaitEvents();
}

}
