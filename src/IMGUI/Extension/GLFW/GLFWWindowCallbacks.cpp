#include "IMGUI/Extension/GLFW/GLFWWindowCallbacks.hpp"
#include "IMGUI/Extension/EventQueue.hpp"
#include "GLFW/glfw3.h"

namespace IMGUI {

namespace {

inline EventQueue& GetEventQueue(GLFWwindow* window) {
  return *reinterpret_cast<EventQueue*>(glfwGetWindowUserPointer(window));
}

void WindowCloseCallback(GLFWwindow* window) {
  glfwSetWindowShouldClose(window,0);
  GetEventQueue(window).PostEvent(Event(Event::Close,0,glm::ivec2(-1,-1)));
}

void WindowPaintCallback(GLFWwindow* window) {
  GetEventQueue(window).PostEvent(Event(Event::Paint,0,glm::ivec2(-1,-1)));
}

void WindowResetCallback(GLFWwindow* window,int /*info*/) {
  GetEventQueue(window).PostEvent(Event(Event::Reset,0,glm::ivec2(-1,-1)));
}

void WindowPosCallback(GLFWwindow* window,int /*width*/,int /*height*/) {
  GetEventQueue(window).PostEvent(Event(Event::Reset,0,glm::ivec2(-1,-1)));
}

void WindowResizeCallback(GLFWwindow* window,int width,int height) {
  GetEventQueue(window).PostEvent(Event(Event::ResizeW,0,glm::ivec2(width,height)));
}

void FramebufferResizeCallback(GLFWwindow* window,int width,int height) {
  GetEventQueue(window).PostEvent(Event(Event::ResizeF,0,glm::ivec2(width,height)));
}

void MouseMoveCallback(GLFWwindow* window,double xpos,double ypos) {
  GetEventQueue(window).PostEvent(Event(Event::MouseMove,0,
                                        glm::ivec2(floor(xpos),floor(ypos))));
}

void MouseButtonCallback(GLFWwindow* window,int button,int action,int /*mods*/) {
  double xpos;
  double ypos;
  glfwGetCursorPos(window,&xpos,&ypos);
  GetEventQueue(window).PostEvent(Event(((action==GLFW_PRESS) ? Event::MouseButtonDown
                                                              : Event::MouseButtonUp),
                                        (1<<button),
                                        glm::ivec2(floor(xpos),floor(ypos))));
}

void MouseWheelCallback(GLFWwindow* window,double xoffset,double yoffset) {
  double xpos;
  double ypos;
  glfwGetCursorPos(window,&xpos,&ypos);
  GetEventQueue(window).PostEvent(Event(Event::MouseWheel,floor(xoffset+yoffset),
                                        glm::ivec2(floor(xpos),floor(ypos))));
}

void KeyCallback(GLFWwindow* window,int key,int /*scancode*/,int action,int mods) {
  int32_t data(key&Event::MaskKey);
  if(mods&GLFW_MOD_ALT)
    data|=Event::Alt;
  if(mods&GLFW_MOD_CONTROL)
    data|=Event::Ctrl;
  if(mods&GLFW_MOD_SHIFT)
    data|=Event::Shift;
  if(mods&GLFW_MOD_SUPER)
    data|=Event::System;
  GetEventQueue(window).PostEvent(Event(((action!=GLFW_RELEASE) ? Event::KeyDown
                                                                : Event::KeyUp),
                                        data,
                                        glm::ivec2()));
}

void TextCallback(GLFWwindow* window,unsigned codepoint) {
  GetEventQueue(window).PostEvent(Event(Event::Text,codepoint,glm::ivec2()));
}

}

void SetGLFWWindowCallbacks(GLFWwindow* window,EventQueue& eventQueue) {
  glfwSetWindowUserPointer(window,&eventQueue);
  glfwSetWindowCloseCallback(window,WindowCloseCallback);
  glfwSetWindowRefreshCallback(window,WindowPaintCallback);
  glfwSetWindowFocusCallback(window,WindowResetCallback);
  glfwSetWindowIconifyCallback(window,WindowResetCallback);
  glfwSetWindowPosCallback(window,WindowResizeCallback);
  glfwSetWindowSizeCallback(window,WindowResizeCallback);
  glfwSetFramebufferSizeCallback(window,FramebufferResizeCallback);
  glfwSetCursorEnterCallback(window,WindowResetCallback);
  glfwSetCursorPosCallback(window,MouseMoveCallback);
  glfwSetMouseButtonCallback(window,MouseButtonCallback);
  glfwSetScrollCallback(window,MouseWheelCallback);
  glfwSetKeyCallback(window,KeyCallback);
  glfwSetCharCallback(window,TextCallback);
}

void ResetGLFWWindowCallbacks(GLFWwindow* window) {
  glfwSetWindowUserPointer(window,nullptr);
  glfwSetWindowCloseCallback(window,nullptr);
  glfwSetWindowRefreshCallback(window,nullptr);
  glfwSetWindowFocusCallback(window,nullptr);
  glfwSetWindowIconifyCallback(window,nullptr);
  glfwSetWindowPosCallback(window,nullptr);
  glfwSetWindowSizeCallback(window,nullptr);
  glfwSetFramebufferSizeCallback(window,nullptr);
  glfwSetCursorEnterCallback(window,nullptr);
  glfwSetCursorPosCallback(window,nullptr);
  glfwSetMouseButtonCallback(window,nullptr);
  glfwSetScrollCallback(window,nullptr);
  glfwSetKeyCallback(window,nullptr);
  glfwSetCharCallback(window,nullptr);
}

}
