#include "IMGUI/Extension/EventQueue.hpp"

namespace IMGUI {

EventQueue::EventQueue() :
  events()
{
}

EventQueue::~EventQueue() {
}

void EventQueue::PostEvent(const Event& event) {
  Event tmpEvent=event;
  // This avoids wrong data in the unlikely case of anyone using EventQueue
  // with SFML, because its not known if its pointing to a reused sf::Event .
  tmpEvent.sfEvent=nullptr;
  events.push_back(tmpEvent);
}

bool EventQueue::PollEvent(Event& event) {
  if(events.empty())
    return false;
  event=events.front();
  events.pop_front();
  return true;
}

bool EventQueue::HasEvent() const {
  return !events.empty();
}

}
