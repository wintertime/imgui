#include "IMGUI/Extension/STB/STBLoadTexture.hpp"
#include "IMGUI/Renderer/IMOGLTexture.hpp"
#include <stdexcept>
#define STB_IMAGE_STATIC
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

namespace IMGUI {

IMOGLTexture STBLoadTexture(const std::string& textureFileName) {
  int x,y,comp;
  stbi_uc* ptr=stbi_load(textureFileName.c_str(),&x,&y,&comp,4);
  if(!ptr)
    throw std::runtime_error("error loading texture "+textureFileName);
  try {
    IMOGLTexture texture{x,y,ptr};
    stbi_image_free(ptr);
    return texture;
  } catch(...) {
    stbi_image_free(ptr);
    throw;
  }
}

}
