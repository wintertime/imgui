#include "IMGUI/Extension/STB/STBMakeTextureFromText.hpp"
#include "IMGUI/Core/UTF8_std_string_decode.hpp"
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"
#include <vector>
#include <stdexcept>

namespace IMGUI {

STBMakeTextureFromText::STBMakeTextureFromText(const char* font_) :
  font(font_),
  fontinfo(std::make_shared<stbtt_fontinfo>())
{
  if(!stbtt_InitFont(fontinfo.get(),(const unsigned char*)font,0))
    throw std::runtime_error("error creating font");
}

IMOGLTexture STBMakeTextureFromText::operator() (const std::string& text) {
  std::u32string codepoints=UTF8_std_string_decode(text);

  std::vector<int> glyphs;
  glyphs.reserve(codepoints.size());
  for(char32_t cp:codepoints)
    glyphs.push_back(stbtt_FindGlyphIndex(fontinfo.get(),cp));

  const size_t imheight=64;
  size_t imwidth=2;
  int ascent;
  int descent;
  int linegap;
  stbtt_GetFontVMetrics(fontinfo.get(),&ascent,&descent,&linegap);
  // ensure linegap is at least 2 pixels high
  if(linegap*(imheight/2-1)<ascent-descent)
    linegap=(ascent-descent+1)/(imheight/2-1);
  int height=ascent-descent+linegap;
  float scale=imheight/(float)height;

  int textwidth=0;
  int textvpos=lrintf(scale*(linegap*0.5f+ascent));
  int texthpos=1;
  std::vector<int> advances;
  advances.reserve(glyphs.size());
  for(size_t i=0;i!=glyphs.size();++i) {
    int advancewidth,leftsidebearing;
    stbtt_GetGlyphHMetrics(fontinfo.get(),glyphs[i],&advancewidth,&leftsidebearing);
    leftsidebearing=lrintf(leftsidebearing*scale);
    if(i!=0)
      advancewidth+=stbtt_GetGlyphKernAdvance(fontinfo.get(),glyphs[i-1],glyphs[i]);
    int advance=lrintf(advancewidth*scale);
    textwidth+=advance;
    advances.push_back(advance);
  }
  advances.push_back(0);
  imwidth+=textwidth;

  size_t imsize=imheight*imwidth*4U;
  unsigned char* image=(unsigned char*)malloc(imsize);
  for(size_t i=0;i<imsize;i+=4) {
    image[i]=0xffU;
    image[i+1]=0xffU;
    image[i+2]=0xffU;
    image[i+3]=0;
  }

  for(size_t i=0;i!=glyphs.size();++i) {
    int bwidth,bheight,xoff,yoff;
    unsigned char* bitmap=stbtt_GetGlyphBitmap(fontinfo.get(),scale,scale,glyphs[i],&bwidth,&bheight,&xoff,&yoff);
    size_t glyphtop=textvpos+yoff;
    size_t glyphleft=texthpos+xoff;
    size_t imrowleft=glyphtop*imwidth+glyphleft;
    for(int iy=0;iy<bheight;++iy,imrowleft+=imwidth) {
      size_t imindex=imrowleft*4+3;
      for(int ix=0;ix<bwidth;++ix,imindex+=4) {
        unsigned alpha=image[imindex];
        alpha+=bitmap[iy*bwidth+ix];
        image[imindex]=(alpha>0xffU) ? 0xffU : alpha;
      }
    }
    stbtt_FreeBitmap(bitmap,nullptr);
    texthpos+=advances[i];
  }

  IMOGLTexture texture{imwidth,imheight,image};
  free(image);
  if(!texture.Get())
    throw std::runtime_error("error creating Texture for text");
  return texture;
}

}
