#include "IMGUI/Renderer/CachedIMOGLRenderer.hpp"

namespace IMGUI {

CachedIMOGLRenderer::CachedIMOGLRenderer(const IRect& screenRect_,TextureCacheBase& textures_,TextureCacheBase& textTextures_) :
  IMOGLRenderer(screenRect_,textures_,textTextures_),
  CachedRenderer()
{
}

CachedIMOGLRenderer::~CachedIMOGLRenderer() {
}

}
