#include "IMGUI/Renderer/IMOGLRenderer.hpp"
#include "IMGUI/Renderer/TextureCacheBase.hpp"
#include "IMGUI/Core/DrawItem.hpp"
#include "GL/gl.h"

namespace IMGUI {

IMOGLRenderer::IMOGLRenderer(const IRect& screenRect_,TextureCacheBase& textures_,TextureCacheBase& textTextures_) :
  screenRect(screenRect_),
  textures(textures_),
  textTextures(textTextures_)
{
}

IMOGLRenderer::~IMOGLRenderer() {
}

void IMOGLRenderer::SetScreenRect(const IRect& screenRect_) {
  screenRect=screenRect_;
}

void IMOGLRenderer::Update(const DrawItem& drawItem) {
  if(drawItem.GetType()==DrawItem::text)
    textTextures.Update(drawItem.GetStr());
}

void IMOGLRenderer::BeginRender() {
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glOrtho(screenRect.pos.x,screenRect.pos.x+screenRect.size.x,screenRect.pos.y+screenRect.size.y,screenRect.pos.y,+1024.0,-1024.0);
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void IMOGLRenderer::Render(const DrawItem& drawItem) {
  switch(drawItem.GetType()) {
  default:
    return;
  case DrawItem::solid:
    {
      const glm::u8vec4& color(drawItem.GetColor());
      glColor4ub(color.r,color.g,color.b,color.a);
      const IRect& rect(drawItem.GetRect());
      glBegin(GL_TRIANGLES);
      glVertex2i(rect.pos.x            ,rect.pos.y);
      glVertex2i(rect.pos.x            ,rect.pos.y+rect.size.y);
      glVertex2i(rect.pos.x+rect.size.x,rect.pos.y+rect.size.y);
      glVertex2i(rect.pos.x+rect.size.x,rect.pos.y+rect.size.y);
      glVertex2i(rect.pos.x+rect.size.x,rect.pos.y);
      glVertex2i(rect.pos.x            ,rect.pos.y);
      glEnd();
      glColor4ub(0,0,0,0);
      return;
    }
  case DrawItem::texture:
    textures.Bind(drawItem.GetStr());
    break;
  case DrawItem::text:
    textTextures.Bind(drawItem.GetStr());
  }

  glEnable(GL_TEXTURE_2D);
  const glm::u8vec4& color(drawItem.GetColor());
  glColor4ub(color.r,color.g,color.b,color.a);
  const IRect& rect(drawItem.GetRect());
  glBegin(GL_TRIANGLES);
  glTexCoord2f(0.f,0.f);
  glVertex2i(rect.pos.x            ,rect.pos.y);
  glTexCoord2f(0.f,1.f);
  glVertex2i(rect.pos.x            ,rect.pos.y+rect.size.y);
  glTexCoord2f(1.f,1.f);
  glVertex2i(rect.pos.x+rect.size.x,rect.pos.y+rect.size.y);
  glVertex2i(rect.pos.x+rect.size.x,rect.pos.y+rect.size.y);
  glTexCoord2f(1.f,0.f);
  glVertex2i(rect.pos.x+rect.size.x,rect.pos.y);
  glTexCoord2f(0.f,0.f);
  glVertex2i(rect.pos.x            ,rect.pos.y);
  glEnd();
  glDisable(GL_TEXTURE_2D);
  if(drawItem.GetType()==DrawItem::texture)
    textures.UnBind();
  else
    textTextures.UnBind();
  glColor4ub(0,0,0,0);
}

void IMOGLRenderer::EndRender() {
  glDisable(GL_BLEND);
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
}

}
