#include "IMGUI/Renderer/CachedRenderer.hpp"

namespace IMGUI {

CachedRenderer::CachedRenderer() :
  SplitRenderer(),
  drawCache()
{
}

CachedRenderer::~CachedRenderer() {
}

void CachedRenderer::Draw(DrawItem&& drawItem) {
  drawCache.Add(std::move(drawItem));
}

void CachedRenderer::ClearCache() {
  drawCache.Clear();
}

void CachedRenderer::UpdateCache() {
  drawCache.Update(*this);
}

void CachedRenderer::RenderCache() {
  drawCache.Draw(*this);
}

}
