#include "IMGUI/Widgets/EditWidgets.hpp"
#include "IMGUI/Core/UTF8_std_string_delete_last.hpp"
#include "IMGUI/Core/UTF8_std_string_add_converted_UTF32_char.hpp"
#include "IMGUI/Core/WidgetLogic.hpp"
#include "IMGUI/Core/DrawBox.hpp"
#include "IMGUI/Core/Frame.hpp"
#include "IMGUI/Core/Event.hpp"

namespace IMGUI {

int32_t EditBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,std::string& text) {
  InvisibleBox(space,bc,size);
  int32_t retval(ButtonLogic(id,frame,space));
  if(!retval) {
    // todo: find out if this is better for reacting to BackSpace:
    // if(frame.event.type==Event::KeyDown && frame.event.data==sf::Keyboard::BackSpace) {
    // }

    if(frame.event.type==Event::Text) {
      // test for backspace character
      if(frame.event.data==0x00000008) {
        retval=-1;
        // remove last character
        UTF8_std_string_delete_last(text);
      } else {
        retval=-2;
        // translate CR to LF and encode the character
        UTF8_std_string_add_converted_UTF32_char(text,frame.event.data==0x0000000d?0x0000000a:frame.event.data);
      }
    }
  }
  DrawButtonBox(frame,style,space,frame.gui.hot==id,frame.gui.active==id);
  DrawTextInBox(frame,style,space,text+"_",frame.gui.hot==id,frame.gui.active==id);
  return retval;
}

}
