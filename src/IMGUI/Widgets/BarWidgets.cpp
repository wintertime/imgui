#include "IMGUI/Widgets/BarWidgets.hpp"
#include "IMGUI/Widgets/ButtonWidgets.hpp"
#include "IMGUI/Core/WidgetLogic.hpp"
#include "IMGUI/Core/Frame.hpp"
#include "IMGUI/Core/Style.hpp"
#include "IMGUI/Core/GenID.hpp"

namespace IMGUI {

namespace {
bool EnforceBounds(int low,int high,int& poslow,int& poshigh) {
  bool changed(false);
  if(low<high) {
    if(poslow<low) {
      poslow=low;
      changed=true;
    } else if(poslow>high) {
      poslow=high;
      changed=true;
    }
    if(poshigh<low) {
      poshigh=low;
      changed=true;
    } else if(poshigh>high) {
      poshigh=high;
      changed=true;
    }
    if(poslow>poshigh) {
      int temp(poslow);
      poslow=poshigh;
      poshigh=temp;
    }
  } else {
    if(poslow<high) {
      poslow=high;
      changed=true;
    } else if(poslow>low) {
      poslow=low;
      changed=true;
    }
    if(poshigh<high) {
      poshigh=high;
      changed=true;
    } else if(poshigh>low) {
      poshigh=low;
      changed=true;
    }
    if(poslow<poshigh) {
      int temp(poslow);
      poslow=poshigh;
      poshigh=temp;
    }
  }
  return changed;
}
}

bool TrackBar(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,int& breadth,int low,int high,int& poslow,int& poshigh) {
  bool changed(EnforceBounds(low,high,poslow,poshigh));
  Style bgstyle(style);
  bgstyle.foreground=bgstyle.background;

  bool horizontal=false;
  if((bc&updownleftright)==0 || (bc&updownleftright)==updownleftright) {
    if(space.size.x>space.size.y)
      horizontal=true;
  } else if((bc&leftright)==leftright || (((bc&up)!=0) != ((bc&down)!=0)))
    horizontal=true;
  if(horizontal) {
    glm::ivec2 size(space.size.x,breadth);
    InvisibleBox(space,bc,size);
    int w0;
    int w2;
    if(poslow>poshigh) {
      w0=(space.size.x*(poshigh-low))/(high-low);
      w2=(space.size.x*(high-poslow))/(high-low);
    } else {
      w0=(space.size.x*(poslow-low))/(high-low);
      w2=(space.size.x*(high-poshigh))/(high-low);
    }
    IRect spaces[3]={
      IRect{space.pos                                            ,glm::ivec2(w0                  ,space.size.y)},
      IRect{glm::ivec2(space.pos.x+w0               ,space.pos.y),glm::ivec2((space.size.x-w2)-w0,space.size.y)},
      IRect{glm::ivec2(space.pos.x+(space.size.x-w2),space.pos.y),glm::ivec2(w2                  ,space.size.y)}
    };
    int posdiff(poshigh-poslow);
    if(Button(GenID(id,3,0),frame,bgstyle,spaces[0],bc,size)) {
      poslow-=posdiff;
      poshigh-=posdiff;
      changed=true;
    }
    if(Button(GenID(id,3,2),frame,bgstyle,spaces[2],bc,size)) {
      poslow+=posdiff;
      poshigh+=posdiff;
      changed=true;
    }
    if(Button(GenID(id,3,1),frame,style,spaces[1],bc,size)) {
      // todo: make real dragging of the knob while holding mousebutton possible
      posdiff=spaces[1].pos.x+spaces[1].size.x/2-frame.mousePos.x;
      if(poslow>poshigh) {
        poslow+=posdiff;
        poshigh+=posdiff;
      } else {
        poslow-=posdiff;
        poshigh-=posdiff;
      }
      changed=true;
    }
  } else {
    glm::ivec2 size(breadth,space.size.y);
    InvisibleBox(space,bc,size);
    int h0;
    int h2;
    if(poslow>poshigh) {
      h0=(space.size.y*(poshigh-low))/(high-low);
      h2=(space.size.y*(high-poslow))/(high-low);
    } else {
      h0=(space.size.y*(poslow-low))/(high-low);
      h2=(space.size.y*(high-poshigh))/(high-low);
    }
    IRect spaces[3]={
      IRect{space.pos                                            ,glm::ivec2(space.size.x,h0                  )},
      IRect{glm::ivec2(space.pos.x,space.pos.y+h0               ),glm::ivec2(space.size.x,(space.size.y-h2)-h0)},
      IRect{glm::ivec2(space.pos.x,space.pos.y+(space.size.y-h2)),glm::ivec2(space.size.x,h2                  )}
    };
    int posdiff(poshigh-poslow);
    if(Button(GenID(id,3,0),frame,bgstyle,spaces[0],bc,size)) {
      poslow-=posdiff;
      poshigh-=posdiff;
      changed=true;
    }
    if(Button(GenID(id,3,2),frame,bgstyle,spaces[2],bc,size)) {
      poslow+=posdiff;
      poshigh+=posdiff;
      changed=true;
    }
    if(Button(GenID(id,3,1),frame,style,spaces[1],bc,size)) {
      // todo: make real dragging of the knob while holding mousebutton possible
      posdiff=spaces[1].pos.y+spaces[1].size.y/2-frame.mousePos.y;
      if(poslow>poshigh) {
        poslow+=posdiff;
        poshigh+=posdiff;
      } else {
        poslow-=posdiff;
        poshigh-=posdiff;
      }
      changed=true;
    }
  }
  return EnforceBounds(low,high,poslow,poshigh) || changed;
}

bool ScrollBar(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,int& breadth,int low,int high,int& poslow,int& poshigh) {
  bool horizontal=false;
  if((bc&updownleftright)==0 || (bc&updownleftright)==updownleftright) {
    if(space.size.x>space.size.y)
      horizontal=true;
  } else if((bc&leftright)==leftright || (((bc&up)!=0) != ((bc&down)!=0)))
    horizontal=true;
  IRect spaces[3]={space,space,space};
  if(horizontal) {
    int arrowwidth;
    if(space.size.x>=3*breadth) {
      arrowwidth=breadth;
    } else {
      arrowwidth=space.size.x/3;
    }
    spaces[0].size.x=arrowwidth;
    spaces[1].pos.x+=arrowwidth;
    spaces[1].size.x-=arrowwidth*2;
    spaces[2].pos.x+=space.size.x-arrowwidth;
    spaces[2].size.x=arrowwidth;
  } else {
    int arrowheight;
    if(space.size.y>=3*breadth) {
      arrowheight=breadth;
    } else {
      arrowheight=space.size.y/3;
    }
    spaces[0].size.y=arrowheight;
    spaces[1].pos.y+=arrowheight;
    spaces[1].size.y-=arrowheight*2;
    spaces[2].pos.y+=space.size.y-arrowheight;
    spaces[2].size.y=arrowheight;
  }
  bool changed(false);
  {
    glm::ivec2 size(spaces[0].size.x,spaces[0].size.y);
    if(Button(GenID(id,3,0),frame,style,spaces[0],bc,size)) {
      if(low<high) {
        --poslow;
        --poshigh;
      } else {
        ++poslow;
        ++poshigh;
      }
      changed=true;
    }
  }
  {
    glm::ivec2 size(spaces[2].size.x,spaces[2].size.y);
    if(Button(GenID(id,3,2),frame,style,spaces[2],bc,size)) {
      if(low<high) {
        ++poslow;
        ++poshigh;
      } else {
        --poslow;
        --poshigh;
      }
      changed=true;
    }
  }
  if(TrackBar(GenID(id,3,1),frame,style,spaces[1],bc,breadth,low,high,poslow,poshigh))
    changed=true;
  if(horizontal) {
    space.pos.y=spaces[1].pos.y;
    space.size.y=spaces[1].size.y;
  } else {
    space.pos.x=spaces[1].pos.x;
    space.size.x=spaces[1].size.x;
  }
  return changed;
}

}
