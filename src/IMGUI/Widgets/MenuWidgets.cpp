#include "IMGUI/Widgets/MenuWidgets.hpp"
#include "IMGUI/Widgets/ButtonWidgets.hpp"
#include "IMGUI/Core/GenID.hpp"

namespace IMGUI {

int32_t Menu(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,const std::string& text,const std::vector<std::string>& itemtext,int& textheight,bool& opened) {
  int32_t titleclick(0);
  IRect titlespace(space);
  glm::ivec2 size(space.size.x,textheight);
  if(TextButton(GenID(id,2,0),frame,style,space,(((bc&maximized)==maximized)?BorderConnect(up|leftright):bc),size,text)) {
    opened=!opened;
    titleclick=-1;
  }
  if(opened) {
    IRect setspace(space);
    int height(space.size.y-textheight);
    size.y=height;
    setspace.size.y=height;
    if(!(bc&down) || (bc&up))
      setspace.pos.y+=textheight;
    int32_t rv(TextButtonSet(GenID(id,2,1),frame,style,space,bc,size,itemtext));
    space.pos.x=setspace.pos.x;
    if(titlespace.pos.y<setspace.pos.y) {
      space.pos.y=titlespace.pos.y;
    } else {
      space.pos.y=setspace.pos.y;
    }
    space.size.x=setspace.size.x;
    space.size.y=titlespace.size.y+setspace.size.y;
    if(rv) {
      opened=false;
      return rv;
    }
  } else
    space=titlespace;
  return titleclick;
}

int32_t TextureMenu(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,const std::string& texture,const std::vector<std::string>& itemtextures,int& textureheight,bool& opened) {
  int32_t titleclick(0);
  IRect titlespace(space);
  glm::ivec2 size(space.size.x,textureheight);
  if(TextureButton(GenID(id,2,0),frame,style,space,(((bc&maximized)==maximized)?BorderConnect(up|leftright):bc),size,texture)) {
    opened=!opened;
    titleclick=-1;
  }
  if(opened) {
    IRect setspace(space);
    int height(space.size.y-textureheight);
    size.y=height;
    setspace.size.y=height;
    if(!(bc&down) || (bc&up))
      setspace.pos.y+=textureheight;
    int32_t rv(TextureButtonSet(GenID(id,2,1),frame,style,space,bc,size,itemtextures)); // todo: change function
    space.pos.x=setspace.pos.x;
    if(titlespace.pos.y<setspace.pos.y) {
      space.pos.y=titlespace.pos.y;
    } else {
      space.pos.y=setspace.pos.y;
    }
    space.size.x=setspace.size.x;
    space.size.y=titlespace.size.y+setspace.size.y;
    if(rv) {
      opened=false;
      return rv;
    }
  } else
    space=titlespace;
  return titleclick;
}

}
