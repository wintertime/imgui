#include "IMGUI/Widgets/ButtonWidgets.hpp"
#include "IMGUI/Core/WidgetLogic.hpp"
#include "IMGUI/Core/Frame.hpp"
#include "IMGUI/Core/Style.hpp"
#include "IMGUI/Core/DrawBox.hpp"
#include "IMGUI/Core/GenID.hpp"

namespace IMGUI {

int32_t Button(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size) {
  InvisibleBox(space,bc,size);
  int32_t triggered(ButtonLogic(id,frame,space));
  DrawButtonBox(frame,style,space,frame.gui.hot==id,frame.gui.active==id);
  return triggered;
}

bool CheckBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,bool& checked) {
  if(Button(id,frame,style,space,bc,size))
    checked=!checked;
  if(checked)
    DrawTextInBox(frame,style,space,"X",frame.gui.hot==id,frame.gui.active==id);
  return checked;
}

int32_t TextButton(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& text) {
  InvisibleBox(space,bc,size);
  int32_t triggered(ButtonLogic(id,frame,space));
  DrawButtonBox(frame,style,space,frame.gui.hot==id,frame.gui.active==id);
  DrawTextInBox(frame,style,space,text,frame.gui.hot==id,frame.gui.active==id);
  return triggered;
}

int32_t TextButton2(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& text,const std::string& text2) {
  InvisibleBox(space,bc,size);
  int32_t triggered(ButtonLogic(id,frame,space));
  DrawButtonBox(frame,style,space,frame.gui.hot==id,frame.gui.active==id);
  DrawTextInBox(frame,style,space,(frame.gui.active==id)?text2:text,frame.gui.hot==id,frame.gui.active==id);
  return triggered;
}

bool TextCheckBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& text,const std::string& text2,bool& checked) {
  if(Button(id,frame,style,space,bc,size))
    checked=!checked;
  DrawTextInBox(frame,style,space,(checked?text2:text),frame.gui.hot==id,frame.gui.active==id);
  return checked;
}

int32_t TextureButton(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& text) {
  InvisibleBox(space,bc,size);
  int32_t triggered(ButtonLogic(id,frame,space));
  DrawTextureInBox(frame,style,space,text,frame.gui.hot==id,frame.gui.active==id);
  return triggered;
}

int32_t TextureButton2(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& texture,const std::string& texture2) {
  InvisibleBox(space,bc,size);
  int32_t triggered(ButtonLogic(id,frame,space));
  DrawTextureInBox(frame,style,space,(frame.gui.active==id)?texture2:texture,frame.gui.hot==id,frame.gui.active==id);
  return triggered;
}

bool TextureCheckBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& texture,const std::string& texture2,bool& checked) {
  InvisibleBox(space,bc,size);
  if(ButtonLogic(id,frame,space))
    checked=!checked;
  DrawTextureInBox(frame,style,space,(checked?texture2:texture),frame.gui.hot==id,frame.gui.active==id);
  return checked;
}

int32_t TextButtonSet(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::vector<std::string>& itemtext) {
  InvisibleBox(space,bc,size);
  if(itemtext.empty()) {
    DrawBackgroundBox(frame,style,space);
    return 0;
  }
  const int sz(itemtext.size());
  IRect spaces[sz+1];
  for(int i(0);i!=sz;++i)
    spaces[i]=IRect{glm::ivec2(space.pos.x,space.pos.y+i*(space.size.y/sz)),glm::ivec2(space.size.x,space.size.y/sz)};
  int32_t triggered(0);
  for(int i(0);i!=sz;++i) {
    int32_t idChild(GenID(id,sz,i));
    if(ButtonLogic(idChild,frame,spaces[i]))
      triggered=i+1;
    DrawButtonBox(frame,style,spaces[i],frame.gui.hot==idChild,frame.gui.active==idChild);
    DrawTextInBox(frame,style,spaces[i],itemtext[i],frame.gui.hot==idChild,frame.gui.active==idChild);
  }
  if(space.size.y%sz) {
    spaces[sz]=IRect{glm::ivec2(space.pos.x,space.pos.y+space.size.y-space.size.y%sz),glm::ivec2(space.size.x,space.size.y%sz)};
    DrawBackgroundBox(frame,style,spaces[sz]);
  }
  return triggered;
}

int32_t TextRadioButtonSet(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::vector<std::string>& itemtext,int32_t& selected) {
  InvisibleBox(space,bc,size);
  if(itemtext.empty()) {
    DrawBackgroundBox(frame,style,space);
    return 0;
  }
  const int sz(itemtext.size());
  IRect spaces[sz+1];
  for(int i(0);i!=sz;++i)
    spaces[i]=IRect{glm::ivec2(space.pos.x,space.pos.y+i*(space.size.y/sz)),glm::ivec2(space.size.x,space.size.y/sz)};
  Style bgstyle(style);
  bgstyle.foreground=bgstyle.background;
  const Style* currentstyle;
  for(int i(0);i!=sz;++i) {
    int32_t idChild(GenID(id,sz,i));
    if(ButtonLogic(idChild,frame,spaces[i])) {
      selected=i+1;
      currentstyle=&style;
    } else
      currentstyle=(selected==i+1)?&style:&bgstyle;
    DrawButtonBox(frame,*currentstyle,spaces[i],frame.gui.hot==idChild,frame.gui.active==idChild);
    DrawTextInBox(frame,*currentstyle,spaces[i],itemtext[i],frame.gui.hot==idChild,frame.gui.active==idChild);
  }
  if(space.size.y%sz) {
    spaces[sz]=IRect{glm::ivec2(space.pos.x,space.pos.y+space.size.y-space.size.y%sz),glm::ivec2(space.size.x,space.size.y%sz)};
    DrawBackgroundBox(frame,style,spaces[sz]);
  }
  return selected;
}

int32_t TextureButtonSet(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::vector<std::string>& itemtextures) {
  InvisibleBox(space,bc,size);
  if(itemtextures.empty()) {
    DrawBackgroundBox(frame,style,space);
    return 0;
  }
  const int sz(itemtextures.size());
  IRect spaces[sz+1];
  for(int i(0);i!=sz;++i)
    spaces[i]=IRect{glm::ivec2(space.pos.x,space.pos.y+i*(space.size.y/sz)),glm::ivec2(space.size.x,space.size.y/sz)};
  int32_t triggered(0);
  for(int i(0);i!=sz;++i) {
    int32_t idChild(GenID(id,sz,i));
    if(ButtonLogic(idChild,frame,spaces[i]))
      triggered=i+1;
    DrawTextureInBox(frame,style,spaces[i],itemtextures[i],frame.gui.hot==idChild,frame.gui.active==idChild);
  }
  if(space.size.y%sz) {
    spaces[sz]=IRect{glm::ivec2(space.pos.x,space.pos.y+space.size.y-space.size.y%sz),glm::ivec2(space.size.x,space.size.y%sz)};
    DrawBackgroundBox(frame,style,spaces[sz]);
  }
  return triggered;
}

int32_t TextureRadioButtonSet(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::vector<std::string>& itemtextures,int32_t& selected) {
  InvisibleBox(space,bc,size);
  if(itemtextures.empty()) {
    DrawBackgroundBox(frame,style,space);
    return 0;
  }
  const int sz(itemtextures.size());
  IRect spaces[sz+1];
  for(int i(0);i!=sz;++i)
    spaces[i]=IRect{glm::ivec2(space.pos.x,space.pos.y+i*(space.size.y/sz)),glm::ivec2(space.size.x,space.size.y/sz)};
  Style bgstyle(style);
  bgstyle.texture*=bgstyle.background; // todo: find better replacement for this line
  const Style* currentstyle;
  for(int i(0);i!=sz;++i) {
    int32_t idChild(GenID(id,sz,i));
    if(ButtonLogic(idChild,frame,spaces[i])) {
      selected=i+1;
      currentstyle=&style;
    } else
      currentstyle=(selected==i+1)?&style:&bgstyle;
    DrawTextureInBox(frame,*currentstyle,spaces[i],itemtextures[i],frame.gui.hot==idChild,frame.gui.active==idChild);
  }
  if(space.size.y%sz) {
    spaces[sz]=IRect{glm::ivec2(space.pos.x,space.pos.y+space.size.y-space.size.y%sz),glm::ivec2(space.size.x,space.size.y%sz)};
    DrawBackgroundBox(frame,style,spaces[sz]);
  }
  return selected;
}

}
