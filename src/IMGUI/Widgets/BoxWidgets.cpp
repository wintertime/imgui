#include "IMGUI/Widgets/BoxWidgets.hpp"
#include "IMGUI/Core/WidgetLogic.hpp"
#include "IMGUI/Core/Frame.hpp"
#include "IMGUI/Core/DrawBox.hpp"

namespace IMGUI {

void BackgroundBox(Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size) {
  InvisibleBox(space,bc,size);
  DrawBackgroundBox(frame,style,space);
}

void ForegroundBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size) {
  InvisibleBox(space,bc,size);
  if(space.contains(frame.mousePos)) // todo: highlight yes/no?
    frame.gui.hot=id;
  DrawForegroundBox(frame,style,space,frame.gui.hot==id);
}

void TextBox(Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& text) {
  BackgroundBox(frame,style,space,bc,size);
  DrawTextInBox(frame,style,space,text,false,false);
}

void ForegroundTextBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& text) {
  ForegroundBox(id,frame,style,space,bc,size);
  DrawTextInBox(frame,style,space,text,frame.gui.hot==id,false);
}

void TextureBox(Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& texture) {
  InvisibleBox(space,bc,size);
  DrawTextureInBox(frame,style,space,texture,false,false);
}

void ForegroundTextureBox(int32_t id,Frame& frame,const Style& style,IRect& space,BorderConnect bc,glm::ivec2& size,const std::string& texture) {
  InvisibleBox(space,bc,size);
  if(space.contains(frame.mousePos))
    frame.gui.hot=id;
  DrawTextureInBox(frame,style,space,texture,frame.gui.hot==id,false);
}

}
